// Set-up Apollo client connection
import {
  ApolloClient,
  createNetworkInterface,
} from 'react-apollo';

// Import configuration files
import config from './config/index';

// Define GraphQL network interface
const networkInterface = createNetworkInterface({
  uri: `${config.graphql.path}/graphql`,
});

// Define Apollo client
const client = new ApolloClient({
  // reduxRootSelector: state => state.apollo,
  networkInterface,
  dataIdFromObject: (o) => o.id,
});

export default client;
