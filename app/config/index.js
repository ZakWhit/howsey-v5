let appConfig = {};

// Set database, port, etc. configurations based on environment
switch (process.env.NODE_ENV) {
  case 'production': {
    appConfig = {
      port: process.env.PORT || 8080,
      graphql: {
        host: process.env.GRAPHQL_URI,
        port: process.env.GRAPHQL_PORT,
      },
    };

    // Determine GraphQL server URI based on environment url
    // Need to determine a more robust solution
    const url = window.location.href;
    if (url.indexOf('prod') > 0) {
      appConfig.graphql.host = 'http://graphql-server-house-app-prod.optasrv.net';
    } else if (url.indexOf('stage') > 0) {
      appConfig.graphql.host = 'http://graphql-server-house-app-stage.optasrv.net';
    }

    if (appConfig.graphql.port) {
      appConfig.graphql.host += `:${appConfig.graphql.port}`;
    }

    appConfig.graphql.path = appConfig.graphql.host;

    break;
  }
  default: {
    appConfig = {
      port: process.env.PORT || 8080,
      graphql: {
        host: 'http://localhost',
        port: 4000,
      },
    };

    if (appConfig.graphql.port) {
      appConfig.graphql.host += `:${appConfig.graphql.port}`;
    }

    appConfig.graphql.path = appConfig.graphql.host;

    break;
  }
}

export const config = appConfig;

export const TOKEN = 'token';

export default config;
