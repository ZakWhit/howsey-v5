/*
 *
 * Explore Page Actions
 *
 */

import {
  EXPLORE_TAB_CHANGE,
} from './constants';

/**
 * Changes the active menu tab index
 *
 * @param  {number} tabIndex The tab index to change to
 *
 * @return {object}    An action object with a type of CHANGE_USERNAME
 */
export function changeExploreTab(tabIndex) {
  return {
    type: EXPLORE_TAB_CHANGE,
    tabIndex,
  };
}
