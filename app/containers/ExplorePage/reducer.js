/*
 * ExplorePageReducer
 */

import { fromJS } from 'immutable';

import {
  EXPLORE_TAB_CHANGE,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  tabIndex: 0,
});

function homeReducer(state = initialState, action) {
  switch (action.type) {
    case EXPLORE_TAB_CHANGE:
      return state
        .set('tabIndex', action.tabIndex);
    default:
      return state;
  }
}

export default homeReducer;
