/*
 * ExplorePage
 *
 * Contains the map view and dashboard for exploring homes and their metrics
 *
 */

import React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage } from 'react-intl';

export class ExplorePage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Helmet
          title="Explore Page"
          meta={[
            { name: 'description', content: 'Explore your neighbourhood and discover our housing price predictions.' },
          ]}
        />
        {React.Children.toArray(this.props.children)}
      </div>
    );
  }
}

ExplorePage.propTypes = {
  children: React.PropTypes.node,
};

export default ExplorePage;
