/**
 * ExplorePage selectors
 */

import { createSelector } from 'reselect';

const selectExplore = (state) => state.get('explore');

const makeSelectTabIndex = () => createSelector(
  selectExplore,
  (exploreState) => exploreState.get('tabIndex')
);

export {
  selectExplore,
  makeSelectTabIndex,
};
