/*
 * Explore Report SlideThree Container
 *
 * Report neighbourhood section container
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { graphql, compose } from 'react-apollo';
import { FetchPlaceData} from './queries';
import { createStructuredSelector } from 'reselect';
import { makeSelectPlace } from 'containers/App/selectors';

// Import components
import SlideThreeComponent from 'components/ReportSlides/SlideThree';

export class SlideThree extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SlideThreeComponent
        place={this.props.place}
        loading={this.props.loading}
        placeData={this.props.placeData}
      />
    );
  }
}

SlideThree.propTypes = {
  place: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
  loading: React.PropTypes.bool,
  placeData: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.any,
  ]),
};

const mapStateToProps = createStructuredSelector({
  place: makeSelectPlace(),
});

const queryOptions = {
  props: ({ ownProps, data }) => ({
    loading: data.loading,
    placeData: data.placeByOak,
  }),
  options(props) {
    return {
      variables: {
        ad_oak: props.place.ad_oak,
      }
    };
  },
};

const SlideThreeComposed = compose(
  connect(
    mapStateToProps,
    null
  ),
  graphql(FetchPlaceData, queryOptions),
)(SlideThree);

export default SlideThreeComposed;
