// Graph QL query to fetch nearby placesGeo
import gql from 'graphql-tag';

export const FetchPlaceData = gql`
  query FetchPlaceData($ad_oak: String!) {
    placeByOak(ad_oak: $ad_oak) {
      architecture_style
      garage_type
      house_price
      house_price_chart {
        dates
        prices
      }
      number_of_bath
      number_of_bedroom
      square_footage
      walkscore
      year_built
    }
  }
`;
