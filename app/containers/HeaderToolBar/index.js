/**
 *
 * HeaderToolBar
 *
 * State container for the Header toolbar
 *
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

// Import components
import { logoutAndRedirect } from 'containers/AuthPage/actions';
import { makeSelectAuthenticated } from 'containers/AuthPage/selectors';
import { makeSelectTabIndex } from 'containers/ExplorePage/selectors';
import ToolBar from 'components/HeaderToolBar';
import AuthDialog from 'components/AuthDialog';

class HeaderToolBar extends Component {
  constructor() {
    super();

    this.handleClose = this.handleClose.bind(this);

    this.state = {
      open: false,
      isRegister: false,
    };
  }

  // Handle the auth dialog open/close state, and auth type state
  handleOpen = (authState) => {
    this.setState({ open: true, isRegister: authState });
  };

  handleClose = () => {
    setTimeout(() => {
      this.setState({ open: false });
    }, 300);
  };

  changeAuth = () => {
    this.setState({ isRegister: !this.state.isRegister });
  };

  render() {
    return (
      <div>
        <ToolBar pathname={this.props.pathname} handleOpen={this.handleOpen} authenticated={this.props.authenticated} tabIndex={this.props.tabIndex} handleLogout={this.props.logout} />
        { (this.state.open) ?
            (
              <AuthDialog
                isRegister={this.state.isRegister}
                handleClose={this.handleClose}
                changeAuth={this.changeAuth}
                authenticated={this.props.authenticated}
              />
            )
          : null }
      </div>
    );
  }
}

HeaderToolBar.propTypes = {
  className: React.PropTypes.string,
  pathname: React.PropTypes.string,
  authenticated: React.PropTypes.bool,
  tabIndex: React.PropTypes.number,
  logout: React.PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  authenticated: makeSelectAuthenticated(),
  tabIndex: makeSelectTabIndex(),
});

export function mapDispatchToProps(dispatch) {
  return {
    logout: () => {
      dispatch(logoutAndRedirect());
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderToolBar);
