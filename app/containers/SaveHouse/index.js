/*
 * SaveHouse Container
 *
 * Saves the current state place to the user profile
 *
 */

import React, { PropTypes } from 'react';

// Import components
import SaveHouseComponent from 'components/SaveHouse';

class SaveHouse extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SaveHouseComponent className={this.props.className} snackbar={this.props.snackbar} tooltipTop={this.props.tooltipTop} />
    );
  }
}

SaveHouse.propTypes = {
  className: PropTypes.string,
  snackbar: PropTypes.bool,
  tooltipTop: PropTypes.bool,
};

export default SaveHouse;
