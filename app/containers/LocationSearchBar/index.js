/*
 *
 * LocationSearchBar
 *
 * This component container controls the location autocomplete form
 */

 /* eslint-disable no-unused-vars */

import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

// Import components
import { fetchGeo } from 'containers/App/actions';
import { makeSelectSearchLocation } from 'containers/App/selectors';
import AutoCompleteForm from 'components/AutoCompleteForm';
import LocationSearchBarItem from 'components/LocationSearchBarItem';
import MenuItem from 'material-ui/MenuItem';
import MarkerIcon from 'material-ui/svg-icons/action/room';

class LocationSearchBar extends Component {
  constructor(props) {
    super(props);

    this.handleUpdateInput = this.handleUpdateInput.bind(this);
    this.handleNewRequest = this.handleNewRequest.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    // dataSource contains formatted results from Places API,
    // data contains the full result from Places API
    const address = (props.searchlocation && props.searchlocation.formatted_address) ? props.searchlocation.formatted_address : '';
    this.state = {
      dataSource: [],
      data: [],
      searchText: address,
    };

    // Fetch Google API services
    this.service = new google.maps.places.AutocompleteService(); // eslint-disable-line no-undef
    this.autocompleteOK = google.maps.places.PlacesServiceStatus.OK; // eslint-disable-line no-undef
  }

  componentWillReceiveProps(nextProps) {
    const address = (nextProps.searchlocation && nextProps.searchlocation.formatted_address) ? nextProps.searchlocation.formatted_address : null;
    if (address) {
      this.setState({
        searchText: address,
      });
    }
  }

  // Called from API callback function to update autocomplete results
  updateDatasource(data) {
    if (!data || !data.length) {
      console.log('No autocomplete results...'); // eslint-disable-line no-console
      this.setState({
        dataSource: [],
      });
    }

    this.setState({
      dataSource: data.map((p) =>
        ({
          text: p.description,
          value: (
            <MenuItem
              className={this.props.menuItemClass}
              leftIcon={<MarkerIcon />}
              primaryText={<LocationSearchBarItem item={p} />}
            />
          ),
        })
      ),
      data,
    });
  }

 // Handle input changes by calling Google API, restrict search to Canada
  handleUpdateInput(value, dataSource, params) {
    if (!value) return;

    this.setState({
      searchText: value,
    });

    // If change comes from a selection do not update dataSource/data state
    if (params.source === 'touchTap') return;

    // Get autocomplete predictions
    this.service.getPlacePredictions(
      { input: value, types: ['address'], componentRestrictions: { country: 'ca' } },
      (predictions, status) => {
        // Clear autocomplete on error case
        if (status !== this.autocompleteOK) {
          this.setState({
            dataSource: [],
          });
          return;
        }

        // Update dataSource state
        this.updateDatasource(predictions);
      }
    );
  }

  // Handle non-autocomplete searches
  handleSubmit() {
    // this.props.fetchSearchData({ address: this.state.searchText });
    const path = (this.props.pathname && (this.props.pathname.indexOf('explore') > -1)) ? this.props.pathname : '/explore/report';
    this.props.onSubmitForm({ address: this.state.searchText }, path);
    return true;
  }

  // Handle autocomplete selection or input field [enter]
  handleNewRequest(searchText, index) {
    // The index in dataSource of the list item selected, or -1 if enter is pressed in the TextField
    if (index === -1) {
      this.handleSubmit();
      return false;
    }
    // Handle autocomplete selection and link to explore page
    const path = (this.props.pathname && (this.props.pathname.indexOf('explore') > -1)) ? this.props.pathname : '/explore/report';
    this.props.onSubmitForm({ placeId: this.state.data[index].place_id }, path);
    return true;
  }

  // Autocomplete results filter function
  caseInsensitiveFilter(searchText, key) {
    return searchText !== '';
  }

  render() {
    return (
      <AutoCompleteForm
        className={this.props.className}
        searchText={this.state.searchText}
        dataSource={this.state.dataSource}
        onUpdateInput={this.handleUpdateInput}
        onNewRequest={this.handleNewRequest}
        filter={this.caseInsensitiveFilter}
        onClick={this.handleSubmit}
        onFocus={this.props.onFocus}
        onBlur={this.props.onBlur}
        onClose={this.props.onClose}
        labelText={this.props.labelText}
        hintText={this.props.hintText}
        btnLabel={this.props.btnLabel}
        autoCompleteProps={this.props.autoCompleteProps}
        buttonProps={this.props.buttonProps}
      />
    );
  }
}

LocationSearchBar.propTypes = {
  className: React.PropTypes.string,
  menuItemClass: React.PropTypes.string,
  labelText: React.PropTypes.object,
  hintText: React.PropTypes.object,
  btnLabel: React.PropTypes.object,
  autoCompleteProps: React.PropTypes.object,
  buttonProps: React.PropTypes.object,
  onSubmitForm: React.PropTypes.func,
  onFocus: React.PropTypes.func,
  onBlur: React.PropTypes.func,
  onClose: React.PropTypes.func,
  flex: React.PropTypes.string,
  searchlocation: React.PropTypes.oneOfType([
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
  pathname: React.PropTypes.string,
};

export function mapDispatchToProps(dispatch) {
  return {
    onSubmitForm: (query, path) => {
      dispatch(fetchGeo(query, path));
    },
  };
}

const mapStateToProps = createStructuredSelector({
  searchlocation: makeSelectSearchLocation(),
});

export default connect(mapStateToProps, mapDispatchToProps)(LocationSearchBar);
