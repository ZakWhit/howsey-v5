/*
 * Map Marker
 *
 * Marker component for map view
 * {(props.primary) ? <MarkerPrimary color={props.color} /> : <Marker color={props.color} /> }
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import { fetchGeo, setPlaceSuccess } from 'containers/App/actions';
import MapMarkerComponent from 'components/MapMarker';
import { createStructuredSelector } from 'reselect';
import { makeSelectPlace } from 'containers/App/selectors';


export class MapMarker extends React.Component {
  constructor(props) {
    super(props);

    this.handleTouchTap = this.handleTouchTap.bind(this);
  }

  handleTouchTap() {
    this.props.onSelectPlace(this.props.place);
  }


  render () {
    const color = (this.props.place.ad_oak === this.props.placePrimary.ad_oak) ? '#d14f27' : '#fff';
    return (
      <MapMarkerComponent {...this.props} color={color} handleTouchTap={this.handleTouchTap}/>
    );
  }

}

MapMarker.propTypes = {
  primary: PropTypes.bool,
  dataId: PropTypes.number,
  place: PropTypes.object,
  lat: PropTypes.number,
  lng: PropTypes.number,
  onSelectPlace: PropTypes.func,
  placePrimary: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
};

const mapStateToProps = createStructuredSelector({
  placePrimary: makeSelectPlace(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onSelectPlace: (place) => {
      dispatch(setPlaceSuccess(place));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MapMarker);
