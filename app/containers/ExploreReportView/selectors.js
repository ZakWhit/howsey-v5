/**
 * ExplorePageReport selectors
 */

import { createSelector } from 'reselect';

const selectReport = (state) => state.get('report');

const makeSelectReportTabIndex = () => createSelector(
  selectReport,
  (reportState) => reportState.get('tabIndex')
);

export {
  selectReport,
  makeSelectReportTabIndex,
};
