/*
 *
 * Explore Page Report Actions
 *
 */

import {
  REPORT_TAB_CHANGE,
} from './constants';

/**
 * Changes the active report tab index
 *
 * @param  {number} tabIndex The tab index to change to
 *
 * @return {object}    An action object with a type of REPORT_TAB_CHANGE
 */
export function changeReportTab(tabIndex) {
  return {
    type: REPORT_TAB_CHANGE,
    tabIndex,
  };
}
