/*
 * Explore Page Report View
 *
 * Report view container
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectLoading, makeSelectError, makeSelectPlace } from 'containers/App/selectors';

// Import components
import ExploreReportViewComponent from 'components/ExploreReportView';

export class ExploreReportView extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ExploreReportViewComponent
        children={this.props.children}
        loading={this.props.loading}
        error={this.props.error}
        place={this.props.place}
        pathname={this.props.location.pathname}
      />
    );
  }
}

ExploreReportView.propTypes = {
  children: React.PropTypes.node,
  loading: React.PropTypes.bool,
  error: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
  place: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
};

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  error: makeSelectError(),
  place: makeSelectPlace(),
});

export default connect(mapStateToProps, null)(ExploreReportView);
