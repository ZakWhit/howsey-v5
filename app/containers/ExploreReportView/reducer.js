/*
 * ExplorePageReportReducer
 */

import { fromJS } from 'immutable';

import {
  REPORT_TAB_CHANGE,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  tabIndex: 0,
});

function reportReducer(state = initialState, action) {
  switch (action.type) {
    case REPORT_TAB_CHANGE:
      return state
        .set('tabIndex', action.tabIndex);
    default:
      return state;
  }
}

export default reportReducer;
