/*
 * AccountButton Container
 *
 * Handles the account button state
 *
 */

import React, { PropTypes } from 'react';

// Import components
import AccountButtonComponent from 'components/AccountButton';

class SaveHouse extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <AccountButton className={this.props.className} snackbar={this.props.snackbar} tooltipTop={this.props.tooltipTop} />
    );
  }
}

SaveHouse.propTypes = {
  className: PropTypes.string,
  snackbar: PropTypes.bool,
  tooltipTop: PropTypes.bool,
};

export default SaveHouse;
