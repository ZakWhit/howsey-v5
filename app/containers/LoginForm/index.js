/**
 *
 * SignUpForm
 *
 * Sign up form container
 *
 */

 /* eslint-disable no-unused-vars */
 /* eslint-disable react/prop-types */

import React, { Component } from 'react';
import { connect } from 'react-redux';

// action to sign in user here
import { createStructuredSelector } from 'reselect';
import { loginUser } from 'containers/AuthPage/actions';
import { makeSelectError, makeSelectAuthenticating, makeSelectAuthenticated, makeSelectToken, makeSelectUser } from 'containers/AuthPage/selectors';

// Import components
import LoginForm from 'components/LoginForm';

class LoginFormContainer extends Component {
  constructor() {
    super();

    this.state = { errors: [] };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(fields) {
    this.props.loginUser(fields);
  }

  render() {
    return (
      <LoginForm
        onSubmit={this.handleSubmit}
        className={this.props.className}
        style={this.props.style}
        errors={this.state.errors}
      />
    );
  }
}

LoginFormContainer.propTypes = {
  className: React.PropTypes.string,
  style: React.PropTypes.object,
  signupUser: React.PropTypes.func,
  error: React.PropTypes.any,
  authenticating: React.PropTypes.bool,
  authenticated: React.PropTypes.bool,
  token: React.PropTypes.any,
  user: React.PropTypes.any,
};

export function mapDispatchToProps(dispatch) {
  return {
    loginUser: (fields) => {
      dispatch(loginUser(fields));
    },
  };
}

const mapStateToProps = createStructuredSelector({
  error: makeSelectError(),
  authenticating: makeSelectAuthenticating(),
  authenticated: makeSelectAuthenticated(),
  token: makeSelectToken(),
  user: makeSelectUser(),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginFormContainer);
