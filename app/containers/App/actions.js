/*
 *
 * App Actions
 *
 * These actions are global to the app
 *
 */

import {
  FETCH_GEO,
  FETCH_GEO_SUCCESS,
  FETCH_GEO_ERROR,
  SET_PLACE,
  SET_PLACE_SUCCESS,
  SET_PLACE_ERROR,
} from './constants';

/**
 * Fetch location search geocode data
 *
 * @param  {object} query The location query object
 * @param  {string} path Path to route to after query is complete
 *
 * @return {object} An action object with a type of FETCH_GEO
 */
export function fetchGeo(query, path=null) {
  return {
    type: FETCH_GEO,
    query,
    path
  };
}

/**
 * Dispatched when the geocode data is fetched by the request saga
 *
 * @param  {object} searchlocation The search location data
 *
 * @return {object}      An action object with a type of FETCH_GEO_SUCCESS passing the repos
 */
export function geoFetched(searchlocation) {
  return {
    type: FETCH_GEO_SUCCESS,
    searchlocation,
  };
}

/**
 * Dispatched when fetching the geocode data fails
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of FETCH_GEO_ERROR passing the error
 */
export function geoFetchingError(error) {
  return {
    type: FETCH_GEO_ERROR,
    error,
  };
}

/**
 * Dispatched when the current place data is fetched by the request saga
 *
 * @param  {object} coords The search location coords
 *
 * @return {object}      An action object with a type of SET_PLACE passing the repos
 */
export function setPlace(coords) {
  return {
    type: SET_PLACE,
    coords,
  };
}

/**
 * Dispatched when the current place data is fetched by the request saga
 *
 * @param  {object} place The place data
 *
 * @return {object}      An action object with a type of SET_PLACE_SUCCESS passing the repos
 */
export function setPlaceSuccess(place) {
  return {
    type: SET_PLACE_SUCCESS,
    place,
  };
}

/**
 * Dispatched when the current place data is fetched by the request saga
 *
 * @param  {object} error The place fetching error
 *
 * @return {object}      An action object with a type of SET_PLACE_ERROR passing the repos
 */
export function setPlaceError(error) {
  return {
    type: SET_PLACE_ERROR,
    error,
  };
}
