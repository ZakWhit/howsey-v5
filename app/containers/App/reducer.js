/*
 * AppReducer
 */

import { fromJS } from 'immutable';

import {
  FETCH_GEO_SUCCESS,
  FETCH_GEO,
  FETCH_GEO_ERROR,
  SET_PLACE,
  SET_PLACE_SUCCESS,
  SET_PLACE_ERROR,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  loading: false,
  error: false,
  searchlocation: false,
  place: false,
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_GEO:
      return state
        .set('loading', true)
        .set('error', false);
    case FETCH_GEO_SUCCESS:
      return state
        .set('searchlocation', action.searchlocation)
        .set('loading', false);
    case FETCH_GEO_ERROR:
      return state
        .set('error', action.error)
        .set('loading', false);
    case SET_PLACE:
      return state
        .set('loading', true)
        .set('error', false);
    case SET_PLACE_SUCCESS:
      return state
        .set('place', action.place)
        .set('loading', false);
    case SET_PLACE_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error);
    default:
      return state;
  }
}

export default appReducer;
