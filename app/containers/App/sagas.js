/**
 * AppSagas
 */

/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */

import { take, call, put, select, cancel, takeLatest } from 'redux-saga/effects';
import { push, LOCATION_CHANGE } from 'react-router-redux';
import { FETCH_GEO, SET_PLACE, REVERSE_FETCH_GEO } from 'containers/App/constants';
import { geoFetched, geoFetchingError, setPlace, setPlaceSuccess, setPlaceError } from 'containers/App/actions';
import { makeSelectLocation } from 'containers/App/selectors';
import {parseJSON, hostUrl, fetchGraphQL} from '../AuthPage/utils';
import { loadState } from 'persist';

// Geocode request by query of form {address: ''} or {place_id: ''}
export function googleGeoCode(query) {
  const geocoder = new google.maps.Geocoder();

  return new Promise((resolve, reject) => {
    geocoder.geocode(query, (results, status) => {
      if (status === google.maps.GeocoderStatus.OK) {
        const searchlocation = {
          formatted_address: results[0].formatted_address,
          lat: results[0].geometry.location.lat(),
          lng: results[0].geometry.location.lng(),
          types: results[0].types,
        };

        if (results[0].place_id) {
          searchlocation.place_id = results[0].place_id;
        } else {
          searchlocation.place_id = null;
        }
        resolve(searchlocation, status);
      } else {
        reject(status);
      }
    });
  });
}

// Geocodes address query, sets the location state, and redirects user ton /explore page
export function* getGeo(action) {
  const query = action.query;
  const path = (action.path) ? action.path : '/explore/report';
  yield put(push(path));
  try {
    // Call our request helper (see 'utils/request')

    const searchlocation = yield call(googleGeoCode, query);
    yield put(geoFetched(searchlocation));
    const coords = {lat: searchlocation.lat, lng: searchlocation.lng};
    yield put({ type: SET_PLACE, coords });
  } catch (err) {
    yield put(geoFetchingError({ error: err }));
  }
}

export function getPlaceByCoords(variables) {
  return new Promise(async (resolve, reject) => {
    const query = `
    query($lat: Float!, $lng: Float!){
       payload: placeGeo(maxDistance: 10, lat: $lat, lng: $lng)
       {
         ad_oak,
         ad_formattedaddress,
         ad_primaryaddress,
         ad_secondaryaddress,
         lat,
         lng,
         streetview
       }
    }`;
    const {error, data} = await fetchGraphQL({query, variables});
    if (error) {
      reject(error);
    } else {
      const { payload } = data;
      resolve(payload);
    }
  });
}

export function* getPlace(action) {
  const coords = action.coords;
  try {
    const place = yield call(getPlaceByCoords, coords);
    yield put(setPlaceSuccess(place));
  } catch (err) {
    yield put(setPlaceError({ error: err }));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export function* geoData() {
  const watcher = yield takeLatest(FETCH_GEO, getGeo);
  const watcherPlace = yield takeLatest(SET_PLACE, getPlace);
  // Suspend execution until location changes
  // yield take(LOCATION_CHANGE);
  // yield cancel(watcher);
}
