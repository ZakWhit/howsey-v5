/*
 *
 * AppConstants
 *
 */

export const FETCH_GEO = 'boilerplate/App/FETCH_GEO';
export const FETCH_GEO_SUCCESS = 'boilerplate/App/FETCH_GEO_SUCCESS';
export const FETCH_GEO_ERROR = 'boilerplate/App/FETCH_GEO_ERROR';
export const SET_PLACE = 'boilerplate/App/SET_PLACE';
export const SET_PLACE_SUCCESS = 'boilerplate/App/SET_PLACE_SUCCESS';
export const SET_PLACE_ERROR = 'boilerplate/App/SET_PLACE_ERROR';
export const DEFAULT_LOCALE = 'en';
