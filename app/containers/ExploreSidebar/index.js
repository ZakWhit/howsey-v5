/*
 * Explore Page Sidebar
 *
 * Explore Sidebar container
 *
 */

import React from 'react';
import { graphql } from 'react-apollo';
import { FetchPlaceData } from './queries';

// Import components
import ExploreSidebarComponent from 'components/ExploreSidebar';

export class ExploreSidebar extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ExploreSidebarComponent place={this.props.place} loading={this.props.loading} placeData={this.props.placeData} report={this.props.report} />
    );
  }
}

ExploreSidebar.propTypes = {
  place: React.PropTypes.object,
  loading: React.PropTypes.bool,
  placeData: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.any,
  ]),
  report: React.PropTypes.bool,
};

const queryOptions = {
  props: ({ ownProps, data }) => ({
    loading: data.loading,
    placeData: data.placeByOak,
  }),
  options(props) {
    return {
      variables: {
        ad_oak: props.place.ad_oak,
      }
    };
  },
};

const ExploreSidebarComposed = graphql(FetchPlaceData, queryOptions)(ExploreSidebar);

export default ExploreSidebarComposed;
