/*
 * Explore Page Map View
 *
 * Map view container
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { graphql, compose } from 'react-apollo';
import { fetchGeo } from 'containers/App/actions';
import { FetchNearbyPlaces } from './queries';
import { createStructuredSelector } from 'reselect';
import { makeSelectSearchLocation, makeSelectLoading, makeSelectError, makeSelectPlace } from 'containers/App/selectors';

// Import components
import ExploreMapViewComponent from 'components/ExploreMapView';

export class ExploreMapView extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.searchThisArea = this.searchThisArea.bind(this);
  }

  searchThisArea(coords) {
    this.props.onSearchThisArea({location: coords}, '/explore/map')
  }

  render() {
    return (
      <ExploreMapViewComponent
        loading={this.props.loading}
        error={this.props.error}
        searchlocation={this.props.searchlocation}
        gqlLoading={this.props.gqlLoading}
        placesNearby={this.props.placesNearby}
        place={this.props.place}
        searchThisArea={this.searchThisArea}
      />
    );
  }
}

ExploreMapView.propTypes = {
  loading: React.PropTypes.bool,
  error: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
  searchlocation: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
  gqlLoading: React.PropTypes.bool,
  placesNearby: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.any,
  ]),
  place: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
  onSearchThisArea: React.PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  searchlocation: makeSelectSearchLocation(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
  place: makeSelectPlace(),
});

// Define Apollo query options
const queryOptions = {
  props: ({ ownProps, data }) => ({
    gqlLoading: data.loading,
    placesNearby: data.placesGeo
  }),
  options(props) {
    return {
      variables: {
        limit: 50,
        maxDistance: 10,
        lat: props.searchlocation.lat,
        lng: props.searchlocation.lng
      }
    };
  },
};

export function mapDispatchToProps(dispatch) {
  return {
    onSearchThisArea: (query, path) => {
      dispatch(fetchGeo(query, path));
    },
  };
}

const ExploreMapViewComposed = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  graphql(FetchNearbyPlaces, queryOptions),
)(ExploreMapView);

export default ExploreMapViewComposed;
