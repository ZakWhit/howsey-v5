// Graph QL query to fetch nearby placesGeo
import gql from 'graphql-tag';

export const FetchNearbyPlaces = gql`
  query FetchNearbyPlaces($limit: Int!, $maxDistance: Int!, $lat: Float!, $lng: Float!) {
    placesGeo(limit: $limit, maxDistance: $maxDistance, lat: $lat, lng: $lng) {
      ad_oak
      ad_formattedaddress
      ad_primaryaddress
      ad_secondaryaddress
      lat
      lng
      distance
      streetview
    }
  }
`;
