// Inline styles for LandingPage component

const styles = {
  autocomplete: {
    display: 'inline-block',
    boxSizing: 'border-box',
    backgroundColor: '#ffffff',
    padding: '0px 20px',
    border: '2px solid #d14f27',
    borderRadius: '50px',
  },
  textfield: {
    boxSizing: 'border-box',
    fontSize: '20px',
    paddingRight: '140px',
  },
  button: {
    position: 'absolute',
    top: 0,
    right: 0,
    height: '100%',
    width: 150,
    background: '#d14f27',
    color: '#fff',
    borderTopRightRadius: '50px',
    borderBottomRightRadius: '50px',
    borderTopLeftRadius: '0px',
    borderBottomLeftRadius: '0px',
    backgroundColor: 'transparent',
  },
  popover: {
    style: {
      marginTop: '5px',
    },
  },
};

export default styles;
