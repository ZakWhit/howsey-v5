/**
 * Landing page main title component
 */

import styled from 'styled-components';

export default styled.h1`
  font-size: 48px;
  line-height: 48px;
  margin-bottom: 0.75em;
  margin-top: 0;
  text-shadow: 0 2px 2px rgba(0,0,0,0.6);
  font-weight: 700;
  color: #ffffff;
`;
