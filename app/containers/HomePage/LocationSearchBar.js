/**
 * LocationSearchBar component
 */

import LocationSearchBar from 'containers/LocationSearchBar';
import styled from 'styled-components';

export default styled(LocationSearchBar)`
  position: relative;
  display: block;
  height: 52px;
  max-width: 700px;
  margin: 0 auto;
  border-radius: 50px;

  & input {
    color: rgb(25, 118, 210) !important;
    text-shadow: 0px 0px 0px #000;
    -webkit-text-fill-color: transparent;
  }

  & input::-webkit-input-placeholder{
    color: #ccc;
    text-shadow: none;
    -webkit-text-fill-color: initial;
  }

  & hr {
    display: none;
  }

  & button {
    border-top-right-radius: 50px !important;
    border-bottom-right-radius: 50px !important;
    border-top-left-radius: 0px !important;
    border-bottom-left-radius: 0px !important;
  }
`;
