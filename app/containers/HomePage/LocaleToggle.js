/**
 * LocaleToggle component
 */

import LocaleToggle from 'containers/LocaleToggle';
import styled from 'styled-components';

export default styled(LocaleToggle)`
  position: absolute;
  bottom: 0;
  right: 15px;
  z-index: 110;
  color: #ffffff;
`;
