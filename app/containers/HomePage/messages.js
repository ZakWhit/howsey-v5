/*
 * LandingPage Messages
 *
 * This contains all the text for the LandingPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'boilerplate.containers.LandingPage.title',
    defaultMessage: 'Discover Your Home\'s Potential',
  },
  label: {
    id: 'boilerplate.containers.LandingPage.label',
    defaultMessage: 'Enter your address',
  },
  placeholder: {
    id: 'boilerplate.containers.LandingPage.placeholder',
    defaultMessage: 'Where do you call home?',
  },
  btnlabel: {
    id: 'boilerplate.containers.LandingPage.btnlabel',
    defaultMessage: 'Search',
  },
});
