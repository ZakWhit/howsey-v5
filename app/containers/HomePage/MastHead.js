/**
 * MastHead component (the landing page video wrapper)
 */

import Paper from 'material-ui/Paper';
import styled from 'styled-components';

export default styled(Paper)`
  position: relative;
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  width: 100%;
  height: 100vh;
`;
