/*
 * LandingPage
 *
 * Web app home page with search bar
 *
 */

import React from 'react';
import Helmet from 'react-helmet';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';

// Import components
import { Grid, Col, Row } from 'components/FlexboxGrid';
import MastHead from './MastHead';
import MastHeadInner from './MastHeadInner';
import LocaleToggle from './LocaleToggle';
import Video from './Video';
import Title from './Title';
import LocationSearchBar from './LocationSearchBar';

// Import messages
import messages from './messages';

// Import media
import droneVideo from './drone_video.mp4';

// Import inline-styles
import styles from './style';

// Define styled components
const SearchGrid = styled(Grid)`
  height: 100%;
`;

const SearchRow = styled(Row)`
  height: 100%;
`;

const SearchCol = styled(Col)`
  padding-bottom: 0px;
`;

// Define LocationSearchBar properties for AutoComplete and RaisedButton components
const autoCompleteProps = {
  menuCloseDelay: 300,
  openOnFocus: true,
  fullWidth: true,
  style: styles.autocomplete,
  textFieldStyle: styles.textfield,
  popoverProps: styles.popover,
};

const buttonProps = {
  primary: true,
  style: styles.button,
};

class HomePage extends React.Component {

  // Since state and props are static,
  // there's no need to re-render this component
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <div style={{ overflow: 'hidden' }}>
        <Helmet
          title="Howsey - Unleashing Your Home's Potential"
          meta={[
            { name: 'description', content: 'Howsey uses predictivce analytics to estimate the price of your home.' },
          ]}
        />
        <Row>
          <MastHead>
            <MastHeadInner>
              <Video src={droneVideo} id="masthead-video" autoPlay height="500" loop="loop" preload="auto" muted />
              <SearchGrid fluid>
                <SearchRow middle="xs" center="xs">
                  <SearchCol xs={12}>
                    <Title><FormattedMessage {...messages.title} /></Title>
                      <LocationSearchBar
                        labelText={<FormattedMessage {...messages.label} />}
                        hintText={<FormattedMessage {...messages.placeholder} />}
                        btnLabel={<FormattedMessage {...messages.btnlabel} />}
                        autoCompleteProps={autoCompleteProps}
                        buttonProps={buttonProps}
                      />
                  </SearchCol>
                </SearchRow>
              </SearchGrid>
            </MastHeadInner>
            <LocaleToggle />
          </MastHead>
        </Row>
      </div>
    );
  }
}

HomePage.propTypes = {
  children: React.PropTypes.node,
};

export default HomePage;
