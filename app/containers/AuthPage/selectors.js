/**
 * The global state selectors
 */

import { createSelector } from 'reselect';

const selectAuth = (state) => state.get('auth');

const makeSelectError = () => createSelector(
  selectAuth,
  (authState) => authState.get('error')
);

const makeSelectAuthenticating = () => createSelector(
  selectAuth,
  (authState) => authState.get('authenticating')
);

const makeSelectAuthenticated = () => createSelector(
  selectAuth,
  (authState) => authState.get('authenticated')
);

const makeSelectToken = () => createSelector(
  selectAuth,
  (authState) => authState.get('token')
);

const makeSelectUser = () => createSelector(
  selectAuth,
  (authState) => authState.get('user')
);

export {
  selectAuth,
  makeSelectError,
  makeSelectAuthenticating,
  makeSelectAuthenticated,
  makeSelectToken,
  makeSelectUser,
};
