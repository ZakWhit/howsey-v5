/**
 * Gets the repositories of the user from Github
 */

/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */

import { take, call, put, select, cancel, takeLatest } from 'redux-saga/effects';
import { push, LOCATION_CHANGE } from 'react-router-redux';
import {parseJSON, hostUrl, fetchGraphQL} from './utils';
import { LOGIN, SIGNUP, LOGOUT } from './constants';
import { loginSuccess, loginError, signupSuccess, signupError  } from './actions';

import { TOKEN } from 'config';

const user = `
{
  id,
  email,
  strategies {
    local {
      isVerified
    }
  }
}`;

const userWithAuthToken = `
{
  user ${user},
  authToken
}`;

export function signupRequest(variables) {
  return new Promise(async (resolve, reject) => {
    const query = `
    mutation($firstname: String!, $lastname: String!, $email: Email!, $password: Password!){
       payload: createUser(firstname: $firstname, lastname: $lastname, email: $email, password: $password)
       ${userWithAuthToken}
    }`;
    const { error, data } = await fetchGraphQL({query, variables});
    if (error) {
      localStorage.removeItem(TOKEN);
      reject(error);
    } else {
      const { payload } = data;
      localStorage.setItem(TOKEN, payload.authToken);
      resolve(payload);
    }
  });
}

export function* signup(action) {
  try {
    const userData = yield call(signupRequest, action.fields);
    yield put(signupSuccess(userData));
  } catch (err) {
    yield put(signupError(err));
  }
}

export function loginRequest(variables) {
  return new Promise(async (resolve, reject) => {
    const query = `
    query($email: Email!, $password: Password!){
       payload: login(email: $email, password: $password)
       ${userWithAuthToken}
    }`;
    const {error, data} = await fetchGraphQL({query, variables});
    if (error) {
      localStorage.removeItem(TOKEN);
      reject(error);
    } else {
      const { payload } = data;
      localStorage.setItem(TOKEN, payload.authToken);
      resolve(payload);
    }
  });
}

export function* login(action) {
  try {
    const userData = yield call(loginRequest, action.fields);
    yield put(signupSuccess(userData));
  } catch (err) {
    yield put(signupError(err));
  }
}

export function* logout(action) {
  try {
    localStorage.removeItem(TOKEN);
    yield put(push('/'));
  } catch (err) {
    console.log(err);
  }
}

export function* authData() {
  const watcherSignup = yield takeLatest(SIGNUP, signup);
  const watcherLogin = yield takeLatest(LOGIN, login);
  const watcherLogout = yield takeLatest(LOGOUT, logout);
}
