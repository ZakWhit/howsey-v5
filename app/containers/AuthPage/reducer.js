/*
 * AppReducer
 */

import { fromJS } from 'immutable';

import {
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  SIGNUP,
  SIGNUP_SUCCESS,
  SIGNUP_ERROR,
  LOGOUT,
  VERIFY_EMAIL_ERROR,
  VERIFY_EMAIL_SUCCESS,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  error: false,
  authenticated: false,
  authenticating: false,
  token: null,
  user: {
    id: null,
    email: null,
    strategies: {},
  },
});

function authReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
    case SIGNUP:
      return state
        .set('error', false)
        .set('authenticating', true);
    case LOGIN_SUCCESS:
    case SIGNUP_SUCCESS:
      return state
        .set('error', false)
        .set('authenticating', false)
        .set('authenticated', true)
        .set('token', action.payload.token)
        .set('user', action.payload.user);
    case LOGIN_ERROR:
    case SIGNUP_ERROR:
    return state
      .set('error', action.error)
      .set('authenticating', false)
      .set('authenticated', false)
      .set('token', null)
      .set('user', {});
    case LOGOUT:
      return initialState;
    case VERIFY_EMAIL_ERROR:
      return state
        .set('error', action.error);
    case VERIFY_EMAIL_SUCCESS:
      return state
        .set('error', false)
        .set('authenticating', false)
        .set('authenticated', true)
        .set('token', action.payload.token)
        .set('user', action.payload.user);
    default:
      return state;
  }
}

export default authReducer;
