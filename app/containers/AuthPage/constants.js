/*
 *
 * AppConstants
 *
 */

export const LOGIN = 'boilerplate/AuthPage/LOGIN_USER';
export const LOGIN_SUCCESS = 'boilerplate/AuthPage/LOGIN_SUCCESS';
export const LOGIN_ERROR = 'boilerplate/AuthPage/LOGIN_ERROR';
export const SIGNUP = 'boilerplate/AuthPage/SIGNUP_USER';
export const SIGNUP_SUCCESS = 'boilerplate/AuthPage/SIGNUP_SUCCESS';
export const SIGNUP_ERROR = 'boilerplate/AuthPage/SIGNUP_ERROR';
export const LOGOUT = 'boilerplate/AuthPage/LOGOUT_USER';
export const VERIFY_EMAIL_ERROR = 'boilerplate/AuthPage/VERIFY_EMAIL_ERROR';
export const VERIFY_EMAIL_SUCCESS = 'boilerplate/AuthPage/VERIFY_EMAIL_SUCCESS';
