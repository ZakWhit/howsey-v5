/*
 *
 * App Actions
 *
 * These actions are global to the app
 *
 */

 import {push, replace} from 'react-router-redux';

import {
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  SIGNUP,
  SIGNUP_SUCCESS,
  SIGNUP_ERROR,
  LOGOUT,
  VERIFY_EMAIL_ERROR,
  VERIFY_EMAIL_SUCCESS,
} from './constants';

/**
 * Signup user request
 *
 * @param  {string} fields Contains login form fields
 *
 * @return {object} An action object with a type of LOGIN
 */
export function loginUser(fields) {
  return {
    type: LOGIN,
    fields,
  };
}

/**
 * Login user success
 *
 * @param  {string} payload Contains user and jwt token data
 *
 * @return {object} An action object with a type of LOGIN_SUCCESS
 */
export function loginSuccess(payload) {
  return {
    type: LOGIN_SUCCESS,
    payload,
  };
}

/**
 * Login user error
 *
 * @param  {string} error Error object
 *
 * @return {object} An action object with a type of LOGIN_ERROR
 */
export function loginError(error) {
  return {
    type: LOGIN_ERROR,
    error,
  };
}

/**
 * Signup user request
 *
 * @param  {string} fields Contains signup form fields
 *
 * @return {object} An action object with a type of SIGNUP
 */
export function signupUser(fields) {
  return {
    type: SIGNUP,
    fields,
  };
}

/**
 * Signup user success
 *
 * @param  {string} payload Contains user and jwt token data
 *
 * @return {object} An action object with a type of SIGNUP_SUCCESS
 */
export function signupSuccess(payload) {
  return {
    type: SIGNUP_SUCCESS,
    payload,
  };
}

/**
 * Signup error
 *
 * @param  {string} error Error object
 *
 * @return {object} An action object with a type of SIGNUP_ERROR
 */
export function signupError(error) {
  return {
    type: SIGNUP_ERROR,
    error,
  };
}

/**
 * Logout user
 *
 * @return {object} An action object with a type of LOGOUT
 */
 export function logoutAndRedirect() {
   return {
     type: LOGOUT,
   };
 }
