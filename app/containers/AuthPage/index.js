/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages.
 *
 */

import React, { Component } from 'react';
import Helmet from 'react-helmet';
import styled, { ThemeProvider } from 'styled-components';

// Import Material-UI components
import {
  cyan500, blue700,
  grey100, grey300, grey400, grey500,
  white, darkBlack, fullBlack,
} from 'material-ui/styles/colors';
import { fade } from 'material-ui/utils/colorManipulator';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import withProgressBar from 'components/ProgressBar';
import Header from 'components/Header';

const styleComponentTheme = {
  fontFamily: 'Roboto, sans-serif',
  palette: {
    primary1Color: '#d14f27',
    primary2Color: '#807F83',
    primary3Color: grey400,
    accent1Color: '#b2b1b4',
    accent2Color: '#175676',
    accent3Color: grey500,
    textColor: '#606060',
    alternateTextColor: white,
    canvasColor: white,
    borderColor: grey300,
    disabledColor: fade(darkBlack, 0.3),
    pickerHeaderColor: cyan500,
    clockCircleColor: fade(darkBlack, 0.07),
    shadowColor: fullBlack,
  },
  appBar: {
    height: 50,
  },
  toolbar: {
    height: 50,
  },
  tabs: {
    backgroundColor: white,
    textColor: '#606060',
    selectedTextColor: '#d14f27',
    accent1Color: '#000',
  }
};

// Set Material UI theme options
const muiTheme = getMuiTheme(styleComponentTheme);

// Style App wrapper div
const AppWrapper = styled.div`
  display: flex;
  min-height: 100%;
  flex-direction: column;
`;

class App extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <ThemeProvider theme={styleComponentTheme}>
        <MuiThemeProvider muiTheme={muiTheme}>
          <AppWrapper>
            <Helmet
              titleTemplate="%s - React.js Boilerplate"
              defaultTitle="React.js Boilerplate"
              meta={[
                { name: 'description', content: 'A React.js Boilerplate application' },
              ]}
            />
            <Header pathname={this.props.location.pathname} />
            {React.Children.toArray(this.props.children)}
          </AppWrapper>
        </MuiThemeProvider>
      </ThemeProvider>
    );
  }
}

App.propTypes = {
  children: React.PropTypes.node,
};

export default App; //withProgressBar(App);
