/*
 * Report Page Stepper container
 *
 * Stepper component displays which report slide is visible
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectReportTabIndex } from 'containers/ExploreReportView/selectors';

// Import components
import ReportStepperComponent from 'components/ReportStepper';

export class ReportStepper extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ReportStepperComponent reportTabIndex={this.props.reportTabIndex} />
    );
  }
}

ReportStepper.propTypes = {
  reportTabIndex: React.PropTypes.number,
};

const mapStateToProps = createStructuredSelector({
  reportTabIndex: makeSelectReportTabIndex(),
});

export default connect(mapStateToProps, null)(ReportStepper);
