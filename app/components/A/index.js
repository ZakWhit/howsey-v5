/**
 * A link to a certain page, an anchor tag
 */

import React from 'react';
import styled from 'styled-components';

const ATag = (props) => <a {...props} />; // eslint-disable-line jsx-a11y/anchor-has-content

const A = styled(ATag)`
  color: #41addd;
  cursor: pointer;

  &:hover {
    color: #6cc0e5;
  }
`;

export default A;
