/**
 * Explore page main row
 */

import { Row } from 'components/FlexboxGrid';
import styled from 'styled-components';

export default styled(Row)`
  position: relative;
  height: 100%;
`;
