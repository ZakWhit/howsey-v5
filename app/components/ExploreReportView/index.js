/*
 * Explore Page Report View
 *
 * Contains the Report view for presenting the place metrics
 *
 */

import React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { RouteTransition } from 'react-router-transition';

// Import components
import { Col } from 'components/FlexboxGrid';
import LoadingIndicator from 'components/LoadingIndicator';
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import { Card } from 'material-ui/Card';
import ExploreSidebar from 'containers/ExploreSidebar';
import ReportStepper from 'containers/ReportStepper';
import Wrapper from './Wrapper';
import RowTall from './RowTall';
import ColTall from './ColTall';
import ToolbarTitle from './ToolbarTitle';

// Import messages
import messages from './messages';

class ExploreReportView extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  render() {
    if (this.props.loading) {
      return (
        <Wrapper fluid>
          <RowTall center="xs" middle="xs">
            <Col xs={12}>
              <LoadingIndicator />
            </Col>
          </RowTall>
        </Wrapper>
      );
    }

    return (
      <Wrapper fluid>
        <RowTall>
          <ColTall className='sidebar-col' md={3} sm={6} xs={12} style={{ zIndex: 10 }}>
            <ExploreSidebar report place={this.props.place} />
          </ColTall>
          <ColTall md={9} sm={6} xs={12}>
            <Card style={{top: 8, left: '2rem', right: '2rem', position: 'absolute'}} zDepth={1}>
              <Toolbar>
                <ToolbarGroup >
                  <ToolbarTitle text={<FormattedMessage {...messages.stepperTitle} />} />
                  <ReportStepper/>
                </ToolbarGroup>
              </Toolbar>
            </Card>
            <RouteTransition
              pathname={this.props.pathname}
              atEnter={{ translateX: 100 }}
              atLeave={{ translateX: -100 }}
              atActive={{ translateX: 0 }}
              mapStyles={styles => ({ transform: `translateX(${styles.translateX}%)` })}
            >
              {this.props.children}
            </RouteTransition>
          </ColTall>
        </RowTall>
      </Wrapper>
    );
  }
}

ExploreReportView.propTypes = {
  children: React.PropTypes.node,
  loading: React.PropTypes.bool,
  error: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
  place: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
  pathname: React.PropTypes.string,
};

export default ExploreReportView;
