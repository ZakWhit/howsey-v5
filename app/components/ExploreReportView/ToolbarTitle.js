/**
 * Explore page content wrapper
 */

import { ToolbarTitle } from 'material-ui/Toolbar';
import styled from 'styled-components';

export default styled(ToolbarTitle)`
  color: #606060;
  font-weight: 500;
`;
