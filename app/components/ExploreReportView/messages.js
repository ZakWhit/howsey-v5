/*
 * ExploreReportView Messages
 *
 * This contains all the text for the ExploreReportView component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  stepperTitle: {
    id: 'boilerplate.components.ExploreReportView.stepperTitle',
    defaultMessage: 'Report:',
  },
});
