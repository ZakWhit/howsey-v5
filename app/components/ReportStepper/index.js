/*
 * Report Page Stepper component
 *
 * Stepper component displays which report slide is visible
 *
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import { browserHistory } from 'react-router';

// Import components
import { Step, Stepper } from 'material-ui/Stepper';
import ValueIcon from 'material-ui/svg-icons/editor/attach-money';
import RiskIcon from 'material-ui/svg-icons/alert/warning';
import LifeIcon from 'material-ui/svg-icons/maps/local-florist';
import InsuranceIcon from 'material-ui/svg-icons/action/verified-user';
import RecsIcon from 'material-ui/svg-icons/action/assignment';
import ArrowForwardIcon from 'material-ui/svg-icons/navigation/arrow-forward';
import StepButton from './StepButton';

// Import Messages
import messages from './messages';

class ReportStepper extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const stepIndex = this.props.reportTabIndex;

    const colorValue = (stepIndex === 0) ? '#d14f27' : '#606060';
    const colorRisk = (stepIndex === 1) ? '#d14f27' : '#606060';
    const colorLife = (stepIndex === 2) ? '#d14f27' : '#606060';
    const colorInsurance = (stepIndex === 3) ? '#d14f27' : '#606060';
    const colorRecs = (stepIndex === 4) ? '#d14f27' : '#606060';

    return (
      <Stepper linear={false} activeStep={stepIndex} style={{height:50, overflowY:'hidden'}} connector={<ArrowForwardIcon />}>
        <Step>
          <StepButton color={colorValue} icon={<ValueIcon color={colorValue} />} onTouchTap={() => browserHistory.push('/explore/report/value')}>
            <FormattedMessage {...messages.step1} />
          </StepButton>
        </Step>
        <Step>
          <StepButton color={colorRisk}  icon={<RiskIcon color={colorRisk} />} onTouchTap={() => browserHistory.push('/explore/report/risk')}>
            <FormattedMessage {...messages.step2} />
          </StepButton>
        </Step>
        <Step>
          <StepButton color={colorLife}  icon={<LifeIcon color={colorLife} />} onTouchTap={() => browserHistory.push('/explore/report/neighbourhood')}>
            <FormattedMessage {...messages.step3} />
          </StepButton>
        </Step>
        <Step>
          <StepButton color={colorInsurance}  icon={<InsuranceIcon color={colorInsurance} />} onTouchTap={() => browserHistory.push('/explore/report/insurance')}>
            <FormattedMessage {...messages.step4} />
          </StepButton>
        </Step>
        <Step>
          <StepButton color={colorRecs}  icon={<RecsIcon color={colorRecs} />} onTouchTap={() => browserHistory.push('/explore/report/recommendations')}>
            <FormattedMessage {...messages.step5} />
          </StepButton>
        </Step>
      </Stepper>
    );
  }
}

ReportStepper.propTypes = {
  reportTabIndex: React.PropTypes.number,
};

export default ReportStepper
