/**
 * StepButton
 */

import { StepButton } from 'material-ui/Stepper';
import styled from 'styled-components';

export default styled(StepButton)`
  & span {
    color: ${(props) => (props.color) ? props.color : 'initial'} !important;
  }
`;
