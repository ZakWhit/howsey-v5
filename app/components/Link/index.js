/**
 * Link component
 */

import { Link } from 'react-router';
import styled from 'styled-components';

const LinkStyled = styled(Link)`
  color: #41addd;

  &:hover {
    color: #6cc0e5;
  }
`;

export default LinkStyled;
