/**
 * Explore page sidebar media image div
 */

import styled from 'styled-components';

export default styled.div`
  width: 100%;
  height: 35vh;
  background: url('${(props) => props.src}') no-repeat center center / cover;
  background-color: #cccccc;
`;
