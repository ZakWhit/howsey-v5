/*
 * Explore Sidebar Component
 *
 * Component contains the explore sidebar place information
 *
 */

import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';

// Import Material UI icons
import CircleIcon from 'material-ui/svg-icons/image/lens';
import ReportIcon from 'material-ui/svg-icons/action/assessment';
import MoreIcon from 'material-ui/svg-icons/action/view-list';
import BedIcon from 'material-ui/svg-icons/maps/local-hotel';
import WCIcon from 'material-ui/svg-icons/notification/wc';
import EventIcon from 'material-ui/svg-icons/action/event';
import FloorIcon from 'material-ui/svg-icons/maps/layers';

// Import components
import { CardMedia, CardTitle, CardText, Card } from 'material-ui/Card';
import Subheader from 'material-ui/Subheader';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import { Col, Row } from 'components/FlexboxGrid';
import LoadingIndicator from 'components/LoadingIndicator';
import IconLabel from 'components/IconLabel';
import RowTall from './RowTall';
import SidebarWrapper from './SidebarWrapper';
import DashboardWrapper from './DashboardWrapper';
import MediaWrapper from './MediaWrapper';
import Media from './Media';
import CardTextComponent from './CardText';
import ReportToolbar from './ReportToolbar';
import HelpIcon from './HelpIcon';
import MoreWrapper from './MoreWrapper';

// Import Messages
import messages from './messages';

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

const PriceDiff = styled.span`
  display: inline-block;
  vertical-align: middle;
  padding-right: 16px;
`;

class ExploreSidebar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
    };
  }

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  estimateTrend(prices) {
    // If no data to create a trend return nothing
    const priceLength = prices.length;
    if (priceLength < 2) return <div />;

    const priceNow = prices[priceLength - 1];
    const pricePrev = prices[priceLength - 2];
    const priceDiff = Math.round(priceNow - pricePrev);

    // Set icon, operator and color based on trend
    const operator = (priceDiff < 0) ? "-" : "+";
    const color = (priceDiff < 0) ? "#cc0000" : "#00b33c";
    function renderIcon(priceDiff) {
      return (priceDiff < 0) ? <TrendDown color={'#cc0000'}/> : <TrendUp color={'#33cc33'}/>
    }

    return(
      <div style={{display:'flex', flexDirection: 'row', alignItems: 'center'}}>
        <h1 style={{margin:0, lineHeight:'1.4em', fontSize: '1.2em', color:'#606060'}}>
          {operator} $ {Math.abs(priceDiff).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} <span style={{color: color}}>({operator} {(Math.round(Math.abs(priceDiff)/prices[priceLength - 2]*100*100)/100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}%)</span>
        </h1>
      </div>
    );
  }

  render() {
    if (this.props.loading) {
      return (
        <RowTall center="xs" middle="xs">
          <Col xs={12}>
            <LoadingIndicator />
          </Col>
        </RowTall>
      );
    }

    const actions = [
      <FlatButton
        label={<FormattedMessage {...messages.close} />}
        primary={true}
        onTouchTap={this.handleClose}
      />,
    ];

    const cardPadding = (this.props.report) ? '28px' : '8px';
    console.log(this.props.placeData)
    return (
      <SidebarWrapper>
        <DashboardWrapper>
          <CardMedia>
            <MediaWrapper>
              <Media src={this.props.place.streetview} />
            </MediaWrapper>
          </CardMedia>
          <CardTitle style={{backgroundColor: '#d14f27'}} title={this.props.place.ad_primaryaddress} subtitle={this.props.place.ad_secondaryaddress} titleStyle={{color: '#fff'}} subtitleStyle={{color: '#fff'}}/>
          <CardText style={{paddingBottom: cardPadding}}>
            <Card style={{position: 'relative'}}>
              {(this.props.report) ?
                  <CardTextComponent style={{position: 'relative',paddingBottom: '36px'}}>
                    <Subheader style={{paddingLeft: 0, fontSize: '1.2em', lineHeight:'1.4em', paddingBottom: '8px'}}>Facts &amp; Features</Subheader>
                    <Row style={{marginBottom:'16px'}}>
                      <Col sm={6} xs={3}>
                        <IconLabel icon={<BedIcon color={'rgb(209, 79, 39)'} style={{height: 30, width: 30}}/>} title='Beds' subtitle={this.props.placeData.number_of_bedroom} />
                      </Col>
                      <Col sm={6} xs={3}>
                        <IconLabel icon={<WCIcon color={'rgb(209, 79, 39)'} style={{height: 30, width: 30}}/>} title='Bathrooms' subtitle={this.props.placeData.number_of_bath} />
                      </Col>
                    </Row>
                    <Row>
                      <Col sm={6} xs={3}>
                        <IconLabel icon={<EventIcon color={'rgb(209, 79, 39)'} style={{height: 30, width: 30}}/>} title='Year Built' subtitle={2007} />
                      </Col>
                      <Col sm={6} xs={3}>
                        <IconLabel icon={<FloorIcon color={'rgb(209, 79, 39)'} style={{height: 30, width: 30}}/>} title='Floor Area' subtitle={`${numberWithCommas(this.props.placeData.square_footage)}  sqft`} />
                      </Col>
                    </Row>
                  </CardTextComponent>
                :
                  <CardTextComponent>
                    <Subheader style={{display:'flex', flexDirection: 'row', alignItems: 'center', color:'#606060', paddingLeft: 0, fontSize: '1.2em', lineHeight:'1.4em', paddingBottom: '8px'}}>
                      <span>{this.props.placeData.number_of_bedroom} <FormattedMessage {...messages.beds}/></span>
                      <span style={{ padding:'0 6px' }}><CircleIcon color={'#606060'} style={{height: 5, width: 5}} /></span>
                      <span>{this.props.placeData.number_of_bath}  <FormattedMessage {...messages.baths}/></span>
                      <span style={{padding:'0 6px'}}><CircleIcon color={'#606060'} style={{height: 5, width: 5}} /></span>
                      <span>{numberWithCommas(this.props.placeData.square_footage)}  <FormattedMessage {...messages.sqft}/></span>
                    </Subheader>
                    <div style={{marginBottom:'8px',}}>
                      <Subheader style={{paddingLeft: 0, fontSize: '1.2em', lineHeight:'1.4em', paddingBottom: '4px'}}>
                        <span><FormattedMessage {...messages.estimate}/>:&nbsp;</span>
                        <HelpIcon color={'rgba(96, 96, 96, 0.54)'} hoverColor={'#d14f27'} onTouchTap={this.handleOpen} />
                      </Subheader>
                      <h1 style={{margin:0, lineHeight:'1.4em'}}>
                        <span style={{color: '#d14f27'}}>$ { numberWithCommas(this.props.placeData.house_price) }</span>
                      </h1>
                    </div>
                    <div style={{marginBottom:'16px',}}>
                      <Subheader style={{paddingLeft: 0, fontSize: '1.2em', lineHeight:'1.4em', paddingBottom: '4px'}}><FormattedMessage {...messages.change}/>:</Subheader>
                      {this.estimateTrend(this.props.placeData.house_price_chart.prices)}
                    </div>
                    <div style={{width: '100%', textAlign: 'center'}}>
                      <Link to="/explore/report">
                        <RaisedButton
                          label={<FormattedMessage {...messages.action}/>}
                          primary
                          icon={<ReportIcon />}
                        />
                      </Link>
                    </div>
                  </CardTextComponent>
              }
              {(this.props.report) ?
                  <MoreWrapper>
                    <FloatingActionButton mini onTouchTap={this.handleOpen}>
                      <MoreIcon />
                    </FloatingActionButton>
                  </MoreWrapper>
                : null
              }
            </Card>
          </CardText>
        </DashboardWrapper>
        <ReportToolbar />
        <Dialog
          title='Howsey Estimate'
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          <FormattedMessage {...messages.dialog} />
        </Dialog>
      </SidebarWrapper>
    );
  }
}

ExploreSidebar.propTypes = {
  place: PropTypes.object,
  loading: PropTypes.bool,
  placeData: PropTypes.object,
  report: PropTypes.bool,
};

export default ExploreSidebar;
