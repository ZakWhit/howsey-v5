/**
 * Explore page dashboard paper component
 */

import ReportToolbar from 'components/ReportToolbar';
import styled from 'styled-components';

export default styled(ReportToolbar)`
  position: absolute;
  top: 8px;
  left: 8px;
  right: 8px;
  height: 48px;
`;
