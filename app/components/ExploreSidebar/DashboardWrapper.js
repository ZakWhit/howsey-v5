/**
 * Explore page sidebar dashboard wrapper
 */

import { Card } from 'material-ui/Card';
import styled from 'styled-components';

export default styled(Card)`
  position: absolute;
  top: 0;
  bottom:0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  overflow-y: scroll;
`;
