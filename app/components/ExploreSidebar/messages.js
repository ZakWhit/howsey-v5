/*
 * ExploreSidebar Messages
 *
 * This contains all the text for the ExploreSidebar component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  close: {
    id: 'boilerplate.components.ExploreSidebar.close',
    defaultMessage: 'Close',
  },
  beds: {
    id: 'boilerplate.components.ExploreSidebar.beds',
    defaultMessage: 'beds',
  },
  baths: {
    id: 'boilerplate.components.ExploreSidebar.baths',
    defaultMessage: 'baths',
  },
  sqft: {
    id: 'boilerplate.components.ExploreSidebar.sqft',
    defaultMessage: 'sqft',
  },
  estimate: {
    id: 'boilerplate.components.ExploreSidebar.estimate',
    defaultMessage: 'Howsey Estimate',
  },
  change: {
    id: 'boilerplate.components.ExploreSidebar.change',
    defaultMessage: 'Last 30 Day Change',
  },
  action: {
    id: 'boilerplate.components.ExploreSidebar.action',
    defaultMessage: 'View Report',
  },
  dialog: {
    id: 'boilerplate.components.ExploreSidebar.dialog',
    defaultMessage: 'The Howsey Estimate is a prediction of your home\'s value. Howsey uses the latest analytical technology to determine the value of your home.',
  },
});
