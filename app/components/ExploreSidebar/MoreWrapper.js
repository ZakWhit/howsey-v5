/**
 * More button wrapper
 */

import styled from 'styled-components';

export default styled.div`
  position:absolute;
  bottom: -20px;
  width: 100%;
  box-sizing: border-box;
  text-align: center;
`;
