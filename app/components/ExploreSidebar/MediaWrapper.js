/**
 * Explore page sidebar media wrapper
 */

import styled from 'styled-components';

export default styled.div`
  width: 100%;
  height: 35vh;
  background-color: #cccccc;
`;
