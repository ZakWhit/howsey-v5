/**
 * Explore page dashboard paper component
 */

import styled from 'styled-components';

export default styled.div`
  position: absolute;
  width: 100%;
  height: calc(100vh - 119px);
  top: 71px;
  left: 0;


  & .show .animate {
    transform: translate(0,0);
  }

`;
