/**
 * Explore page dashboard paper component
 */

import styled from 'styled-components';

export default styled.div`
  position: relative;
  width: 125px;
  height: 125px;
`;
