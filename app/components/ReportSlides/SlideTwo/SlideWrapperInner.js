/**
 * Explore page dashboard paper component
 */

import styled from 'styled-components';

export default styled.div`
  position: relative;
  display:inline-block
  height: 100%;
  width: 100%;
  padding-bottom: 50px;
  overflowY: scroll;
  overflowX: hidden;
`;
