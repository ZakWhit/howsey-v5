/*
 * Explore Page Report View SlideTwo Component
 *
 * Contains the report risk slide content
 *
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';

// Import Material UI icons
import LockIcon from 'material-ui/svg-icons/action/lock';
import RiskIcon from 'material-ui/svg-icons/image/lens';

// Import components
import { Grid, Row, Col } from 'components/FlexboxGrid';
import { Card, CardHeader, CardText } from 'material-ui/Card';
import { ToolbarSeparator } from 'material-ui/Toolbar';
import Subheader from 'material-ui/Subheader';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import SlideWrapper from './SlideWrapper';
import SlideWrapperInner from './SlideWrapperInner';
import RiskCard from './RiskCard';
import HelpIcon from './HelpIcon';

// Import messages
import messages from './messages';

const PriceDiff = styled.span`
  padding-right: 16px;
`;

const Separator = styled(ToolbarSeparator)`
  & {
    margin: 0 10px;
  }
`;

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

class SlideOne extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      title: '',
      content: '',
    };

    this.handleToggle = this.handleToggle.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleToggle = (title,content) => {
    this.setState({
      open: !this.state.open,
      title: title,
      content: content,
    });
  }

  handleClose = () => this.setState({open: false});

  render() {
    if (!this.props.placeData) {
      return null;
    }

    const actions = [
      <FlatButton
        label="Close"
        primary={true}
        onTouchTap={this.handleClose}
      />,
    ];

    return (
      <SlideWrapper >
        <SlideWrapperInner >
          <Grid fluid >
            <Row style={{marginBottom: 16}}>
              <Col xs={12}>
                <Card>
                  <CardHeader style={{paddingBottom: 0}} titleStyle={{lineHeight: '48px', fontSize: '20px'}} title='Risk Analysis' subtitle='Insurance companies use a methodology called risk assessment to calculate premium rates for policyholders. Using software that computes a predetermined algorithm, insurance underwriters gauge the risk that you may file a claim against your policy.'/>
                  <CardText>
                    <FlatButton
                      label="Low Risk"
                      default
                      icon={<RiskIcon color='#00b33c' />}
                      disabled
                      labelStyle={{color: 'rgb(96, 96, 96)'}}
                    />
                    <FlatButton
                      label="Moderate Risk"
                      default
                      icon={<RiskIcon color='#ffcc00' />}
                      disabled
                      labelStyle={{color: 'rgb(96, 96, 96)'}}
                    />
                    <FlatButton
                      label="High Risk"
                      default
                      icon={<RiskIcon color='#cc0000' />}
                      disabled
                      labelStyle={{color: 'rgb(96, 96, 96)'}}
                    />
                  </CardText>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col sm={6} xs={12} style={{marginBottom: 16}}>
                <RiskCard handleToggle={this.handleToggle} riskType='Fire' riskScore={23} riskMessage='Fire presents significant risk to businesses. It can kill or seriously injure employees or visitors and can damage or destroy buildings, equipment and stock.' iconClassName='fa-fire' />
              </Col>
              <Col sm={6} xs={12} style={{marginBottom: 16}}>
                <RiskCard handleToggle={this.handleToggle} riskType='Water' riskScore={66} riskMessage='Fire presents significant risk to businesses. It can kill or seriously injure employees or visitors and can damage or destroy buildings, equipment and stock.' iconClassName='fa-tint' />
              </Col>
              <Col sm={6} xs={12} style={{marginBottom: 16}}>
                <RiskCard handleToggle={this.handleToggle} riskType='Sewer Backup' riskScore={82} riskMessage='Fire presents significant risk to businesses. It can kill or seriously injure employees or visitors and can damage or destroy buildings, equipment and stock.' iconClassName='fa-ban' />
              </Col>
              <Col sm={6} xs={12} style={{marginBottom: 16}}>
                <RiskCard handleToggle={this.handleToggle} riskType='Crime' riskScore={9} riskMessage='Fire presents significant risk to businesses. It can kill or seriously injure employees or visitors and can damage or destroy buildings, equipment and stock.' iconClassName='fa-gavel' />
              </Col>
            </Row>
          </Grid>
          <Dialog
            title={`${this.state.title} Exposure`}
            actions={actions}
            modal={false}
            open={this.state.open}
            onRequestClose={this.handleClose}
          >
            {this.state.content}
          </Dialog>
        </SlideWrapperInner >

      </SlideWrapper >
    );
  }
}

SlideOne.propTypes = {
  place: React.PropTypes.object,
  loading: React.PropTypes.bool,
  placeData: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.any,
  ]),
};

export default SlideOne;
