/*
 * Explore Page Report View SlideThree Component
 *
 * Contains the report neighbourhhod slide content
 *
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';

// Import Material UI icons
import LockIcon from 'material-ui/svg-icons/action/lock';
import RiskIcon from 'material-ui/svg-icons/image/lens';
import WalkIcon from 'material-ui/svg-icons/maps/directions-walk';
import TransitIcon from 'material-ui/svg-icons/maps/directions-bus';
import BikeIcon from 'material-ui/svg-icons/maps/directions-bike';

// Import components
import { Grid, Row, Col } from 'components/FlexboxGrid';
import { Card, CardHeader, CardText } from 'material-ui/Card';
import { ToolbarSeparator } from 'material-ui/Toolbar';
import Subheader from 'material-ui/Subheader';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import HoverCard from 'components/HoverCard';

import {
  Table,
  TableBody,
  TableFooter,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

import SlideWrapper from './SlideWrapper';
import SlideWrapperInner from './SlideWrapperInner';
import RiskCard from './RiskCard';
import HelpIcon from './HelpIcon';

// Import messages
import messages from './messages';

const PriceDiff = styled.span`
  padding-right: 16px;
`;

const Separator = styled(ToolbarSeparator)`
  & {
    margin: 0 10px;
  }
`;

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

class SlideThree extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      title: '',
      content: '',
    };

    this.handleToggle = this.handleToggle.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleToggle = (title,content) => {
    this.setState({
      open: !this.state.open,
      title: title,
      content: content,
      fixedHeader: true,
    });
  }

  handleClose = () => this.setState({open: false});


  render() {
    if (!this.props.placeData) {
      return null;
    }

    const actions = [
      <FlatButton
        label="Close"
        primary={true}
        onTouchTap={this.handleClose}
      />,
    ];

    const tableData = [
      {
        rating: 6,
        name: 'William Armstrong Public School',
        grades: 'SK-8',
        distance: '4.2km'
      },
      {
        rating: 6,
        name: 'Saint Joseph Catholic School',
        grades: 'SK-8',
        distance: '7.6km'
      },
      {
        rating: 6,
        name: 'Markham District High School',
        grades: '9-12',
        distance: '5.1km'
      },
    ];

    return (
      <SlideWrapper >
        <SlideWrapperInner >
          <Grid fluid >
            <Row style={{marginBottom: 16}}>
              <Col xs={12}>
                <Card>
                  <CardHeader style={{paddingBottom: 0}} titleStyle={{lineHeight: '48px', fontSize: '20px'}} title={`Neighbourhood: L6B1A8`} subtitle={'This neighbourhood is located in the NE end of Markham, ON. The total population of Markham is 321,034 with an median household income of $96,300.'}/>
                  <CardText style={{paddingTop:0}}></CardText>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col xs={4} style={{marginBottom: 16}}>
                <RiskCard handleToggle={this.handleToggle} riskType='Walk Score' riskScore={64} riskMessage='Fire presents significant risk to businesses. It can kill or seriously injure employees or visitors and can damage or destroy buildings, equipment and stock.' scoreMessage='Daily errands do not require a car.' icon={<WalkIcon />} />
              </Col>
              <Col xs={4} style={{marginBottom: 16}}>
                <RiskCard handleToggle={this.handleToggle} riskType='Transit Score' riskScore={21} riskMessage='Fire presents significant risk to businesses. It can kill or seriously injure employees or visitors and can damage or destroy buildings, equipment and stock.' scoreMessage='World-class public transportation' icon={<TransitIcon />} />
              </Col>
              <Col xs={4} style={{marginBottom: 16}}>
                <RiskCard handleToggle={this.handleToggle} riskType='Bike Score' riskScore={92} riskMessage='Fire presents significant risk to businesses. It can kill or seriously injure employees or visitors and can damage or destroy buildings, equipment and stock.' scoreMessage='Very steep hills, excellent bike lanes' icon={<BikeIcon />} />
              </Col>
            </Row>
            <Row style={{marginBottom: 16}}>
              <Col xs={12}>
                <Card>
                  <CardHeader titleStyle={{lineHeight: '48px', fontSize: '20px'}} title={`Nearby Schools in Markham`} actAsExpander={true} showExpandableButton={true}/>
                  <CardText style={{paddingTop:0}} expandable={true}>
                    <Table
                      fixedHeader={true}
                      selectable={false}
                      multiSelectable={false}
                    >
                      <TableHeader
                        displaySelectAll={false}
                        adjustForCheckbox={false}
                      >
                        <TableRow>
                          <TableHeaderColumn>Scool Rating</TableHeaderColumn>
                          <TableHeaderColumn>Name</TableHeaderColumn>
                          <TableHeaderColumn>Grades</TableHeaderColumn>
                          <TableHeaderColumn>Distance</TableHeaderColumn>
                        </TableRow>
                      </TableHeader>
                      <TableBody
                        displayRowCheckbox={false}
                        showRowHover={true}
                      >
                        {tableData.map( (row, index) => (
                          <TableRow key={index} style={{cursor:'pointer'}}>
                            <TableRowColumn>{row.rating}/10</TableRowColumn>
                            <TableRowColumn>{row.name}</TableRowColumn>
                            <TableRowColumn>{row.grades}</TableRowColumn>
                            <TableRowColumn>{row.distance}</TableRowColumn>
                          </TableRow>
                          ))}
                      </TableBody>
                    </Table>
                    <div style={{width:'100%',textAlign:'right',paddingTop:'16px'}}>
                      <FlatButton backgroundColor='#d14f27' hoverColor="#df7353" labelStyle={{color: '#fff'}} label="View on Map" onTouchTap={this.handleTouchTap} />
                    </div>
                  </CardText>
                </Card>
              </Col>
            </Row>
            <Row style={{marginBottom: 16}}>
              <Col xs={12}>
                <Card>
                  <CardHeader titleStyle={{lineHeight: '48px', fontSize: '20px'}} title={`Nearby Religious Institutions in Markham`} actAsExpander={true} showExpandableButton={true}/>
                  <CardText style={{paddingTop:0}} expandable={true}>
                    <Table
                      fixedHeader={true}
                      selectable={false}
                      multiSelectable={false}
                    >
                      <TableHeader
                        displaySelectAll={false}
                        adjustForCheckbox={false}
                      >
                        <TableRow>
                          <TableHeaderColumn>Scool Rating</TableHeaderColumn>
                          <TableHeaderColumn>Name</TableHeaderColumn>
                          <TableHeaderColumn>Grades</TableHeaderColumn>
                          <TableHeaderColumn>Distance</TableHeaderColumn>
                        </TableRow>
                      </TableHeader>
                      <TableBody
                        displayRowCheckbox={false}
                        showRowHover={true}
                      >
                        {tableData.map( (row, index) => (
                          <TableRow key={index} style={{cursor:'pointer'}}>
                            <TableRowColumn>{row.rating}/10</TableRowColumn>
                            <TableRowColumn>{row.name}</TableRowColumn>
                            <TableRowColumn>{row.grades}</TableRowColumn>
                            <TableRowColumn>{row.distance}</TableRowColumn>
                          </TableRow>
                          ))}
                      </TableBody>
                    </Table>
                    <div style={{width:'100%',textAlign:'right',paddingTop:'16px'}}>
                      <FlatButton backgroundColor='#d14f27' hoverColor="#df7353" labelStyle={{color: '#fff'}} label="View on Map" onTouchTap={this.handleTouchTap} />
                    </div>
                  </CardText>
                </Card>
              </Col>
            </Row>
          </Grid>
          <Dialog
            title={`${this.state.title} Exposure`}
            actions={actions}
            modal={false}
            open={this.state.open}
            onRequestClose={this.handleClose}
          >
            {this.state.content}
          </Dialog>
        </SlideWrapperInner >

      </SlideWrapper >
    );
  }
}

SlideThree.propTypes = {
  place: React.PropTypes.object,
  loading: React.PropTypes.bool,
  placeData: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.any,
  ]),
};

export default SlideThree;
