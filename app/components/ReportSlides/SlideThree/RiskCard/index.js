/**
 *
 * Report Slide Risk Card
 *
 * Contains the information regarding a particuar risk
 *
 */

import React from 'react';

// Import components
import { Card, CardText } from 'material-ui/Card';
import SubHeader from 'material-ui/Subheader';
import FontIcon from 'material-ui/FontIcon';
import FlatButton from 'material-ui/FlatButton';
import RadialBarChart from 'components/RadialBarChart';

class Chart extends React.Component {
  constructor(props) {
    super(props);

    this.handleTouchTap = this.handleTouchTap.bind(this);
  }

  handleTouchTap(){
    this.props.handleToggle(this.props.riskType, this.props.riskMessage)
  }

  render() {
    const riskScore = this.props.riskScore;

    let paneColor = '';
    let barColor = '';
    if ((riskScore >= 0) && (riskScore < 40)) {
      paneColor = '#ff6666';
      barColor = '#cc0000';
    } else if ((riskScore >= 40) && (riskScore < 70)) {
      paneColor = '#ffeb99';
      barColor = '#ffcc00';
    } else {
      paneColor = '#c2f0c2';
      barColor = '#33cc33';
    }


    const config = {
      chart: {
        type: 'solidgauge',
        margin: 0,
        height: 125
      },
      title: {
        text: '',
        style: {
          display: 'none'
        }
      },

      pane: {
        startAngle: 0,
        endAngle: 360,
        background: [{ // Track for Move
          outerRadius: '112%',
          innerRadius: '88%',
          backgroundColor: paneColor,
          borderWidth: 0
        }]
      },
      yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: []
      },
      plotOptions: {
        solidgauge: {
          dataLabels: {
            enabled: false
          },
          linecap: 'round',
          stickyTracking: false,
          rounded: true
        }
      },
      series: [{
        name: 'Monthly Change',
        data: [{
          color: barColor,
          radius: '112%',
          innerRadius: '88%',
          y: Math.abs(riskScore)
        }]
      }],
      credits: {
          enabled: false
      },
    }

    return (
      <Card>
        <CardText>
          <div style={{display:'flex', flexDirection: 'row', alignItems: 'flex-start'}}>
            <div style={{width:'calc(100% - 125px)'}}>
              <SubHeader style={{display:'flex', alignItems:'center', paddingLeft: 0, fontSize: '1.2em', lineHeight:'1.4em', paddingBottom:0 }}>
                <div style={{ marginRight:'8px' }}>
                  {this.props.icon}
                </div>
                {this.props.riskType}
              </SubHeader>
              <CardText style={{paddingLeft: 0}}>
                {this.props.scoreMessage}
              </CardText>
              <FlatButton backgroundColor='#d14f27' hoverColor="#df7353" labelStyle={{color: '#fff'}} label="Learn More" onTouchTap={this.handleTouchTap} />
            </div>
            <div style={{height:125,width:125, textAlign: 'center', zIndex:20}}>
              <RadialBarChart height={'125px'} width={'125px'} config={config} barColor={barColor} riskScore={riskScore}/>
            </div>
          </div>
        </CardText>
      </Card>
    );
  }
}

Chart.propTypes = {
  icon: React.PropTypes.node,
  riskScore: React.PropTypes.number,
  riskType: React.PropTypes.string,
  riskMessage: React.PropTypes.string,
  scoreMessage: React.PropTypes.string,
  handleToggle: React.PropTypes.func,
};

export default Chart;
