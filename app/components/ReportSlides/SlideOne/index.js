/*
 * Explore Page Report View SlideTwo Component
 *
 * Contains the report value slide content
 *
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';

// Import Material UI icons
import LockIcon from 'material-ui/svg-icons/action/lock';

// Import components
import { Grid, Row, Col } from 'components/FlexboxGrid';
import { Card, CardText } from 'material-ui/Card';
import { ToolbarSeparator } from 'material-ui/Toolbar';
import Subheader from 'material-ui/Subheader';

import SlideWrapper from './SlideWrapper';
import SlideWrapperInner from './SlideWrapperInner';
import HoverCard from 'components/HoverCard';
import PriceChart from './PriceChart'
import HelpIcon from './HelpIcon';

// Import messages
import messages from './messages';

const PriceDiff = styled.span`
  padding-right: 16px;
`;

const Separator = styled(ToolbarSeparator)`
  & {
    margin: 0 10px;
  }
`;

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

class SlideTwo extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

  }

  estimateTrend(prices, home) {
    // If no data to create a trend return nothing
    const priceLength = prices.length;
    if (priceLength < 2) return <div />;

    const priceNow = (home) ? prices[priceLength - 1] : (prices[priceLength - 1]/1.1);
    const pricePrev = prices[priceLength - 2];
    const priceDiff = Math.round(priceNow - pricePrev);

    // Set icon, operator and color based on trend
    const operator = (priceDiff < 0) ? "-" : "+";
    const color = (priceDiff < 0) ? "#cc0000" : "#00b33c";
    function renderIcon(priceDiff) {
      return (priceDiff < 0) ? <TrendDown color={'#cc0000'}/> : <TrendUp color={'#33cc33'}/>
    }

    return(
      <div style={{display:'flex', flexDirection: 'row', alignItems: 'center'}}>

        <h1 style={{margin:0, lineHeight:'1.4em', fontSize: '1.2em', color:'#606060'}}>
          {operator} $ {Math.abs(priceDiff).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} <span style={{color: color}}>({operator} {(Math.round(Math.abs(priceDiff)/prices[priceLength - 2]*100*100)/100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}%)</span>
        </h1>
      </div>
    );
  }

  render() {
    if (!this.props.placeData) {
      return null;
    }
    return (
      <SlideWrapper >
        <SlideWrapperInner >
          <Grid fluid >
            <Row style={{marginBottom: 16}}>
              <Col sm={6} xs={12}>
                <HoverCard style={{marginBottom: '15px'}}>
                  <CardText>
                    <div style={{textAlign:'center',}}>
                      <Subheader style={{paddingLeft: 0, fontSize: '1.2em', lineHeight:'1.4em', paddingBottom: '4px'}}>
                        <span>Howsey Estimate:&nbsp;</span>
                        <HelpIcon color={'rgba(96, 96, 96, 0.54)'} hoverColor={'#d14f27'} />
                      </Subheader>
                      <h1 style={{margin:0, lineHeight:'1.4em'}}>
                        <span style={{color: '#d14f27'}}>$ { numberWithCommas(this.props.placeData.house_price) }</span>
                      </h1>
                    </div>
                  </CardText>
                  <CardText style={{paddingTop:0}}>
                    <div style={{display:'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', flexWrap:'wrap'}}>
                      <div>
                        <div>
                          <Subheader style={{paddingLeft: 0, fontSize: '1.2em', lineHeight:'1.4em', paddingBottom: '4px'}}>Last 30 Day Change:</Subheader>
                          {this.estimateTrend(this.props.placeData.house_price_chart.prices,true)}
                        </div>
                      </div>
                      <div>
                        <Separator />
                      </div>
                      <div>
                        <Subheader style={{ paddingLeft: 0, fontSize: '1.2em', lineHeight:'1.4em', paddingBottom: '4px'}}>
                          <span>Estimate Range:</span>
                        </Subheader>
                        <h1 style={{margin:0, lineHeight:'1.4em', fontSize: '1.2em'}}>
                          <span style={{color: '#606060'}}>$ { numberWithCommas(this.props.placeData.house_price - 23000) } - $ { numberWithCommas(this.props.placeData.house_price + 43000) }</span>
                        </h1>
                      </div>
                    </div>
                  </CardText>
                </HoverCard>
              </Col>
              <Col sm={6} xs={12}>
                  <HoverCard style={{marginBottom: '15px'}}>
                    <CardText>
                      <div style={{textAlign:'center',}}>
                        <Subheader style={{paddingLeft: 0, fontSize: '1.2em', lineHeight:'1.4em', paddingBottom: '4px'}}>
                          <span>Neighbourhood Estimate:&nbsp;</span>
                          <HelpIcon color={'rgba(96, 96, 96, 0.54)'} hoverColor={'#d14f27'} />
                        </Subheader>
                        <h1 style={{margin:0, lineHeight:'1.4em'}}>
                          <span style={{color: '#d14f27'}}>$ { numberWithCommas(Math.round(this.props.placeData.house_price / 1.3)) }</span>
                        </h1>
                      </div>
                    </CardText>
                    <CardText style={{paddingTop:0}}>
                      <div style={{display:'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', flexWrap:'wrap'}}>
                        <div>
                          <div>
                            <Subheader style={{paddingLeft: 0, fontSize: '1.2em', lineHeight:'1.4em', paddingBottom: '4px'}}>Last 30 Day Change:</Subheader>
                            {this.estimateTrend(this.props.placeData.house_price_chart.prices,false)}
                          </div>
                        </div>
                        <div>
                          <Separator />
                        </div>
                        <div>
                          <Subheader style={{ paddingLeft: 0, fontSize: '1.2em', lineHeight:'1.4em', paddingBottom: '4px'}}>
                            <span>Estimate Range:</span>
                          </Subheader>
                          <h1 style={{margin:0, lineHeight:'1.4em', fontSize: '1.2em'}}>
                            <span style={{color: '#606060'}}>$ { numberWithCommas(Math.round(this.props.placeData.house_price/1.3) - 23000) } - $ { numberWithCommas(Math.round(this.props.placeData.house_price/1.3) + 43000) }</span>
                          </h1>
                        </div>
                      </div>
                    </CardText>
                  </HoverCard>
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                  <HoverCard style={{marginBottom: '15px'}}>
                    <CardText style={{textAlign: 'center'}}>
                      <Subheader style={{paddingLeft: 0, fontSize: '1.2em', lineHeight:'1.2em', paddingBottom: '0'}}>Historical Home Value Trend</Subheader>
                    </CardText>
                    <div style={{display:'flex', flexDirection: 'row', alignItems: 'flex-start'}}>
                      <div style={{width:'calc(100% - 100px)'}}>
                        {(this.props.placeData && this.props.placeData.house_price_chart) ? <PriceChart priceData={this.props.placeData.house_price_chart}/> : null}
                      </div>
                      <div style={{height:180,width:100, textAlign: 'center', marginRight:15, backgroundColor:'#d14f27',display:'flex',zIndex:20}}>
                        <div style={{alignSelf: 'center',cursor:'pointer',padding:'10px 0'}}>
                          <LockIcon color='#fff' style={{height:40,width:40}} />
                          <Subheader style={{color:'#fff',paddingLeft: 0, fontSize: '1em', lineHeight:'1.2em'}}>Unlock Forecast</Subheader>
                        </div>
                      </div>
                    </div>
                  </HoverCard>
              </Col>
            </Row>
          </Grid>
        </SlideWrapperInner >
      </SlideWrapper >
    );
  }
}

SlideTwo.propTypes = {
  place: React.PropTypes.object,
  loading: React.PropTypes.bool,
  placeData: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.any,
  ]),
};

export default SlideTwo;
