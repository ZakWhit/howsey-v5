/*
 * ReportStepper Messages
 *
 * This contains all the text for the ReportStepper component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  step1: {
    id: 'boilerplate.components.ReportStepper.step1',
    defaultMessage: 'Value',
  },
  step2: {
    id: 'boilerplate.components.ReportStepper.step2',
    defaultMessage: 'Risk',
  },
  step3: {
    id: 'boilerplate.components.ReportStepper.step3',
    defaultMessage: 'Neighbourhood',
  },
  step4: {
    id: 'boilerplate.components.ReportStepper.step4',
    defaultMessage: 'Insurance',
  },
  step5: {
    id: 'boilerplate.components.ReportStepper.step5',
    defaultMessage: 'Recommendations',
  },
});
