/*
 * Explore Page Report View
 *
 * Contains the report deck slides for the current property
 *
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';

// Import components
import ReactHighcharts from 'react-highcharts';

class PriceChart extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

  }

  shouldComponentUpdate(e) {
    return false;
  }

  render () {
    const neighbourhoodData = this.props.priceData.prices.map((x) => {
      let random = Math.random()
      let addition = ( random < 0.5) ? random : -( 1 - random);
      return x * ( 1 + addition);
    });

    const config = {
      chart: {
        height: 300,
      },
      title: {
          text: ''
      },
      xAxis: {
        title: {
          text: 'Date'
        },
        categories: this.props.priceData.dates,
    
      },
      yAxis: {
        title: {
          text: 'Value ($CAD)'
        },
      },
      series: [{
        name: 'Howsey Estimate',
        data: this.props.priceData.prices
      },
      {
        name: 'Neighbourhood Estimate',
        data: neighbourhoodData,
        visible: false,
        dashStyle: 'longdash',
        color: 'rgba(0,0,0,0.6)',
      }],
      tooltip: {
        pointFormat: '$ {point.y:,.0f}',
      },
      credits: {
        enabled: false
      },
      legend: {
        enabled: true,
      },
    };



    return (
      <ReactHighcharts config={config}/>
    );

  }


}

PriceChart.propTypes = {
  priceData: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.any,
  ]),
};

export default PriceChart;
