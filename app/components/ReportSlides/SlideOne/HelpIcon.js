/**
 * Explore page dashboard paper component
 */

import HelpIcon from 'material-ui/svg-icons/action/help-outline';
import styled from 'styled-components';

export default styled(HelpIcon)`
   height: 23px !important;
   width: 19px !important;
   display: inline-block !important;
   verticalAlign: middle !important;
   cursor: pointer;
   border-radius: 50%;
`;
