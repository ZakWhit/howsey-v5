/*
 * HoverCard Component
 *
 * CCard that changes zDepth level on mouse enter
 *
 */

import React from 'react';

// Import components
import { Card } from 'material-ui/Card';

class HoverCard extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);

    this.state = { shadow: 1 } //, translate: 'translate(0,-2px)' }
  }

  onMouseEnter = () => {
    this.setState({ shadow: 2 }) //, translate: 'translate(0,-2px)' })
  };
  onMouseLeave = () => {
    this.setState({ shadow: 1 }) //, translate: 'translate(0,0)' })
  };

  render() {
    const pointer = (this.props.pointer) ? 'pointer' : 'initial';
    return (
      <div style={{cursor: pointer}} onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave}>
        <Card onTouchTap={this.props.onTouchTap} zDepth={this.state.shadow}>
          {React.Children.toArray(this.props.children)}
        </Card>
      </div>
    );
  }

}

HoverCard.propTypes = {
  children: React.PropTypes.node,
  onTouchTap: React.PropTypes.func,
  pointer: React.PropTypes.bool,
};

export default HoverCard;
