/**
 *
 * AutoCompleteForm
 *
 * Renders an autocomplete form containing a Material UI autocomplete component,
 * and a submit button component
 *
 */

import React, { PropTypes } from 'react';

// Import components
import AutoComplete from 'material-ui/AutoComplete';
import FlatButton from 'material-ui/FlatButton';
import Search from 'material-ui/svg-icons/action/search';

function AutoCompleteForm(props) {
  return (
    <div className={props.className}>
      <label htmlFor="location" className="hidden">{props.labelText}</label>
      <AutoComplete
        className='autocomplete'
        searchText={props.searchText}
        hintText={props.hintText}
        dataSource={props.dataSource}
        onUpdateInput={props.onUpdateInput}
        onNewRequest={props.onNewRequest}
        filter={props.filter}
        onFocus={props.onFocus}
        onBlur={props.onBlur}
        onClose={props.onClose}
        {...props.autoCompleteProps}
      />
      <FlatButton
        secondary
        onClick={props.onClick}
        label={props.btnLabel}
        icon={<Search color={'#fff'} />}
        {...props.buttonProps}
      />
    </div>
  );
}

AutoCompleteForm.propTypes = {
  className: React.PropTypes.string,
  labelText: PropTypes.object,
  hintText: PropTypes.object,
  autoCompleteProps: PropTypes.object,
  btnLabel: PropTypes.object,
  buttonProps: PropTypes.object,
  searchText: PropTypes.string,
  dataSource: PropTypes.array,
  onUpdateInput: PropTypes.func,
  onNewRequest: PropTypes.func,
  filter: PropTypes.func,
  onClick: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onClose: PropTypes.func,
};

export default AutoCompleteForm;
