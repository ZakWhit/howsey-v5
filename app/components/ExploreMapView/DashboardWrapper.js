/**
 * Explore page dashboard paper component
 */

import { Card } from 'material-ui/Card';
import styled from 'styled-components';

export default styled(Card)`
  position: relative;
  display: inline-block;
  height: 100%;
  width: 100%;
  padding: 0 1em;
`;
