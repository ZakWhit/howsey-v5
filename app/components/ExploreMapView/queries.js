// Graph QL query to fetch nearby placesGeo
import gql from 'graphql-tag';

export const FetchNearbyPlaces = gql`
  query FetchNearbyPlaces($limit: Int!, $maxDistance: Int!, $ge_latitude: Float!, $ge_longitude: Float!) {
    placesGeo(limit: $limit, maxDistance: $maxDistance, ge_latitude: $ge_latitude, ge_longitude: $ge_longitude) {
      ad_oak
      ad_streetaddress
      ad_municipality
      ad_province
      ge_latitude
      ge_longitude
      prk_purchase_price
    }
  }
`;
