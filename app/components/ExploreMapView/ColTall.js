/**
 * Explore page main columns
 */

import { Col } from 'components/FlexboxGrid';
import styled from 'styled-components';

export default styled(Col)`
  position: relative;
  height: 100%;
  padding-right: 0;
  padding-left: 0;
  overflow: hidden;

  background: #f2f2f2;
`;
//boxShadow: rgba(0, 0, 0, 0.12) 0px 1px 2px, rgba(0, 0, 0, 0.12) 0px 1px 4px;
