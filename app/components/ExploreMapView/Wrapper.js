/**
 * Explore page content wrapper
 */

import { Grid } from 'components/FlexboxGrid';
import styled from 'styled-components';

export default styled(Grid)`
  position: absolute;
  box-sizing: border-box;
  width: 100%;
  top: 48px;
  left: 0;
  right: 0;
  bottom: 0;
  padding-left: 0.5em;
  padding-right: 0.5em;
  overflow-y: hidden;
`;
