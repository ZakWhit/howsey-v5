/*
 * Explore Page Map View
 *
 * Contains the map view for exploring homes and their metrics
 *
 */

import React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage } from 'react-intl';

// Import components
import ExploreMap from 'components/ExploreMap';
import LoadingIndicator from 'components/LoadingIndicator';
import { Col } from 'components/FlexboxGrid';
import ExploreSidebar from 'containers/ExploreSidebar';

import Wrapper from './Wrapper';
import RowTall from './RowTall';
import ColTall from './ColTall';

// Import messages
import messages from './messages';

function ExploreMapView({ loading, error, searchlocation, gqlLoading, placesNearby, place, searchThisArea }) { // eslint-disable-line react/prefer-stateless-function
  if (loading || gqlLoading) {
    return (
      <Wrapper fluid>
        <RowTall center="xs" middle="xs">
          <Col xs={12}>
            <LoadingIndicator />
          </Col>
        </RowTall>
      </Wrapper>
    );
  }

  return (
    <Wrapper fluid>
      <RowTall>
        <ColTall md={3} sm={6} xs={12} style={{ zIndex: 10}}>
          <ExploreSidebar place={place} />
        </ColTall>
        <ColTall md={9} sm={6} xs={12}>
          <ExploreMap
            coords={{lat: searchlocation.lat, lng: searchlocation.lng}}
            placesNearby={placesNearby}
            searchThisArea={searchThisArea}
          />
        </ColTall>
      </RowTall>
    </Wrapper>
  );
}

ExploreMapView.propTypes = {
  loading: React.PropTypes.bool,
  error: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
  searchlocation: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
  place: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
  gqlLoading: React.PropTypes.bool,
  placesNearby: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.any,
  ]),
  searchThisArea: React.PropTypes.func,
};

export default ExploreMapView;
