/**
 * MapMarker ToolTip Card
 */

import { Card } from 'material-ui/Card';
import styled from 'styled-components';

export default styled(Card)`
  position: absolute;
  opacity: ${(props) => props.opacity} ;
  transition: opacity 0.3s ease-in-out !important;
  -webkit-transition: opacity 0.3s ease-in-out !important;
  -moz-transition: opacity 0.3s ease-in-out !important;
  -ms-transition: opacity 0.3s ease-in-out !important;
  -o-transition: opacity 0.3s ease-in-out !important;
  width: 300px;
  z-index: 60;
`;
