/*
 * Map Marker
 *
 * Marker component for map view
 * {(props.primary) ? <MarkerPrimary color={props.color} /> : <Marker color={props.color} /> }
 */

import React, { PropTypes } from 'react';

import IconButton from 'material-ui/IconButton';
import MarkerPrimary from 'material-ui/svg-icons/image/adjust';
import Marker from 'material-ui/svg-icons/image/brightness-1';
import { CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import Card from './Card';
import { MARKER_SIZE } from './constants';

export class MapMarker extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      opacity: 0,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.$hover) {
      this.setState({
        opacity: 1,
      });
    } else {
      this.setState({
        opacity: 0,
      });
    }
  }

  render() {
    const {$dimensionKey,$geoService,$getDimensions,$hover,$onMouseAllow,$prerender, lat, lng} = this.props;
    let markerWrapper = Object.assign({}, {$dimensionKey,$geoService,$getDimensions,$hover,$onMouseAllow,$prerender, lat, lng})

    let cardOpacity = ($hover) ? 1 : 0;
    let markerZIndex = ($hover) ? 51 : 'initial';
    let markerColor = ($hover) ? '#d14f27' : this.props.color;

    return (
      <markerWrapper style={{position: 'absolute', width: MARKER_SIZE, height: MARKER_SIZE, left: -MARKER_SIZE / 2, top: -MARKER_SIZE / 2, zIndex: `${markerZIndex}`}}>
        <IconButton
          onTouchTap={this.props.handleTouchTap}
          style={{zIndex:0, padding: 8, boxSizing: 'border-box',width: MARKER_SIZE,height: MARKER_SIZE,}}
          iconStyle={{cursor: 'pointer', border: `1px solid ${this.props.color}`, borderRadius: '50%', width: MARKER_SIZE - 16, height: MARKER_SIZE - 16,}}
        >
          <Marker color={markerColor} />
        </IconButton>
        <Card opacity={cardOpacity}>
          <CardHeader
            style={{paddingRight: 0}}
            title={this.props.place.ad_primaryaddress}
            subtitle={this.props.place.ad_secondaryaddress}
            avatar={this.props.place.streetview}
          />
        </Card>
      </markerWrapper>
    );
  }
}

MapMarker.propTypes = {
  primary: PropTypes.bool,
  color: PropTypes.string,
  dataId: PropTypes.number,
  place: PropTypes.object,
  lat: PropTypes.number,
  lng: PropTypes.number,
  handleTouchTap: PropTypes.func,
};

export default MapMarker;
