// Inline styles for auth dialog component

const styles = {
  authModal: {
    width: '100%',
    maxWidth: 400,
  },
  button: {
    marginBottom: 10,
    width: '100%',
  },
};

export default styles;
