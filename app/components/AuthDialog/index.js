/**
 *
 * AuthDialog
 *
 * The modal that appears when a user clicks on the Login or Signup menu item
 * Contains Login/SignUp options
 *
 */

import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';

// Import components
import Dialog from 'material-ui/Dialog';
import Divider from 'material-ui/Divider';
import A from 'components/A';
import Link from 'components/Link';
import TextSeparator from 'components/TextSeparator';
import LoginForm from 'containers/LoginForm';
import SignUpForm from 'containers/SignUpForm';
import SocialWrapper from './SocialWrapper';
import SocialButton from './SocialButton';
import AuthPrompt from './AuthPrompt';
import ForgotPassword from './ForgotPassword';

// Import messages
import messages from './messages';

// Import media
import facebook from './facebook-logo.png';
import google from './google-plus-logo.png';

// Import inline styles
import styles from './style';

class AuthDialog extends Component {
  constructor() {
    super();

    this.state = {
      localOpen: true,
    };
  }

  // Handles closing the dialog box
  handleClose = () => {
    this.setState({ localOpen: false });
    this.props.handleClose();
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.localOpen && nextProps.authenticated) {
      console.log('closing')
      this.handleClose();
    }
  }

  render() {
    // Set text properties based on whether in signup/login state, determined by bool isRegister
    const dialogTitle = (this.props.isRegister) ? (<FormattedMessage {...messages.signuptitle} />)
                          : (<FormattedMessage {...messages.logintitle} />);
    const authPrompt = (this.props.isRegister) ? (<FormattedMessage {...messages.signupauthprompt} />)
                          : (<FormattedMessage {...messages.loginauthprompt} />);
    const authLink = (this.props.isRegister) ? (<FormattedMessage {...messages.signupauthlink} />)
                          : (<FormattedMessage {...messages.loginauthlink} />);

    return (
      <Dialog
        title={<h3>{dialogTitle}</h3>}
        contentStyle={styles.authModal}
        actions={null}
        modal={false}
        open={this.state.localOpen}
        onRequestClose={this.handleClose}
        autoScrollBodyContent
      >
        <SocialWrapper>
          <SocialButton
            label={<FormattedMessage {...messages.facebookbtn} />}
            labelPosition="after"
            primary={false}
            icon={<img alt="facebook-icon" src={facebook} height={14} />}
            labelColor="#FFFFFF"
            backgroundColor="#3B5998"
          />
          <SocialButton
            label={<FormattedMessage {...messages.googlebtn} />}
            labelPosition="after"
            primary={false}
            icon={<img alt="google-icon" src={google} height={14} />}
            labelColor="#FFFFFF"
            backgroundColor="#DB3236"
          />
        </SocialWrapper>
        <TextSeparator width={'100%'} text={'OR'} />
        {(this.props.isRegister) ?
            (
              <div>
                <SignUpForm style={{ marginTop: 10 }} />
              </div>
            )
          :
            (
              <div>
                <LoginForm style={{ marginTop: 10 }} />
                <ForgotPassword>
                  <Link to="/forgot-password"><FormattedMessage {...messages.forgot} /></Link>
                </ForgotPassword>
              </div>
            )
        }
        <Divider />
        <AuthPrompt>
          <span>{authPrompt} <A role="button" onTouchTap={this.props.changeAuth}>{authLink}</A></span>
        </AuthPrompt>
      </Dialog>
    );
  }
}

AuthDialog.propTypes = {
  isRegister: React.PropTypes.bool,
  handleClose: React.PropTypes.func,
  changeAuth: React.PropTypes.func,
  authenticated: React.PropTypes.bool,
};

export default AuthDialog;
