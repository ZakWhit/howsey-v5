/**
 * RaisedButton containing with social authorization option
 */

import RaisedButton from 'material-ui/RaisedButton';
import styled from 'styled-components';

export default styled(RaisedButton)`
  margin-bottom: 10px;
  width: 100%;
`;
