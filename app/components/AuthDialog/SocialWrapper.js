/**
 * Div that wraps the SocialButton components
 */

import styled from 'styled-components';

export default styled.div`
  display: block;
  text-align: center;
  margin-top: 15px;
  margin-bottom: 15px;
`;
