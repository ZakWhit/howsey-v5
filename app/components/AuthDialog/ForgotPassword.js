/**
 * Div containing the forgot password prompt
 */

import styled from 'styled-components';

export default styled.div`
  text-align: center;
  margin-top: 15px;
  margin-bottom: 15px;
`;
