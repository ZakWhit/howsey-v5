/**
 * Div containing the prompt to switch between Login/Signup state
 */

import styled from 'styled-components';

export default styled.div`
  text-align: center;
  margin-top: 15px;
`;
