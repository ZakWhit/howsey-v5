/*
 * AuthDialog Messages
 *
 * This contains all the text for the AuthDialog component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  signuptitle: {
    id: 'boilerplate.components.AuthDialog.signuptitle',
    defaultMessage: 'Sign Up and Explore Howsey!',
  },
  logintitle: {
    id: 'boilerplate.components.AuthDialog.logintitle',
    defaultMessage: 'Login to Your Howsey Account!',
  },
  signupauthprompt: {
    id: 'boilerplate.components.AuthDialog.signupauthprompt',
    defaultMessage: 'Already have an account?',
  },
  loginauthprompt: {
    id: 'boilerplate.components.AuthDialog.loginauthprompt',
    defaultMessage: 'Don\'t have an account?',
  },
  signupauthlink: {
    id: 'boilerplate.components.AuthDialog.signupauthlink',
    defaultMessage: 'Log In',
  },
  loginauthlink: {
    id: 'boilerplate.components.AuthDialog.loginauthlink',
    defaultMessage: 'Sign Up',
  },
  facebookbtn: {
    id: 'boilerplate.components.AuthDialog.facebookbtn',
    defaultMessage: 'Continue with Facebook',
  },
  googlebtn: {
    id: 'boilerplate.components.AuthDialog.googlebtn',
    defaultMessage: 'Continue with Google',
  },
  forgot: {
    id: 'boilerplate.components.AuthDialog.forgot',
    defaultMessage: 'Forgot Password?',
  },
});
