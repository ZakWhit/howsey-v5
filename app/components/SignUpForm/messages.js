/*
 * RegisterForm Messages
 *
 * This contains all the text for the RegisterForm component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  first: {
    id: 'boilerplate.components.SignUpForm.first',
    defaultMessage: 'First Name',
  },
  last: {
    id: 'boilerplate.components.SignUpForm.last',
    defaultMessage: 'Last Name',
  },
  email: {
    id: 'boilerplate.components.SignUpForm.email',
    defaultMessage: 'Email',
  },
  password: {
    id: 'boilerplate.components.SignUpForm.password',
    defaultMessage: 'Password',
  },
  submit: {
    id: 'boilerplate.components.SignUpForm.submit',
    defaultMessage: 'Sign Up',
  },
});
