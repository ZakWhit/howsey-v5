/**
 * Explore page dashboard paper component
 */

import SaveHouse from 'containers/SaveHouse';
import styled from 'styled-components';

export default styled(SaveHouse)`
  cursor: pointer;
  height: 48px !important;
  width: 48px !important;
`;
