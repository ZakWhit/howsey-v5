/*
 * ReportStepper Messages
 *
 * This contains all the text for the ReportStepper component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  owner: {
    id: 'boilerplate.components.ReportToolbar.owner',
    defaultMessage: 'Owner',
  },
  edit: {
    id: 'boilerplate.components.ReportToolbar.edit',
    defaultMessage: 'Edit Info',
  },
  share: {
    id: 'boilerplate.components.ReportToolbar.share',
    defaultMessage: 'Share',
  },
  menu1: {
    id: 'boilerplate.components.ReportToolbar.menu1',
    defaultMessage: 'More Info',
  },
  menu2: {
    id: 'boilerplate.components.ReportToolbar.menu2',
    defaultMessage: 'Report',
  },
});
