/*
 * Explore Sidebar Toolbar
 *
 * Toolbar in the explore sidebar
 *
 */

import React, { PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';

// Import Material UI icons
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';
import ShareIcon from 'material-ui/svg-icons/social/share';
import HomeIcon from 'material-ui/svg-icons/action/home';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';

// Import components
import { Card } from 'material-ui/Card';
import { Toolbar, ToolbarGroup, ToolbarSeparator } from 'material-ui/Toolbar';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import SaveHouse from './SaveHouse';

import messages from './messages';

class ReportToolbar extends React.Component {
  constructor(props) {
    super(props);
  }

  handleChange = (event, index, value) => this.setState({value});

  render() {
    const tooltipPosition = (this.props.tooltipTop) ? 'top-center' : 'bottom-center';
    return (
      <Card className={this.props.className} zDepth={1}>
        <Toolbar>
          <ToolbarGroup firstChild>
            <SaveHouse snackbar tooltipTop={this.props.tooltipTop} />
            <ToolbarSeparator style={{marginLeft: 4}} />
          </ToolbarGroup>
          <ToolbarGroup lastChild>
            <IconButton tooltip={<FormattedMessage {...messages.owner} />} tooltipPosition={tooltipPosition}>
              <HomeIcon />
            </IconButton>
            <IconButton tooltip={<FormattedMessage {...messages.edit} />} tooltipPosition={tooltipPosition}>
              <EditIcon />
            </IconButton>
            <IconButton tooltip={<FormattedMessage {...messages.share} />} tooltipPosition={tooltipPosition}>
              <ShareIcon />
            </IconButton>
            <IconMenu
              iconButtonElement={
                <IconButton touch={true}>
                  <NavigationExpandMoreIcon />
                </IconButton>
              }
            >
              <MenuItem primaryText={<FormattedMessage {...messages.menu1} />} />
              <MenuItem primaryText={<FormattedMessage {...messages.menu2} />} />
            </IconMenu>
          </ToolbarGroup>
        </Toolbar>
      </Card>
    );
  }
}

ReportToolbar.propTypes = {
  className: PropTypes.string,
  tooltipTop: PropTypes.bool,
};

export default ReportToolbar;
