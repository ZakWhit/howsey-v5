/**
 *
 * Logo Img
 *
 * Renders the Howsey logo
 *
 */

import React from 'react';

// Import components
import Img from 'components/Img';

// Import media
import logo from './logo.png';
import logoHome from './logo_light.png';

function LogoLink(props) {
  let src = (props.home) ? logoHome : logo;
  return (
    <Img className={props.className} src={src} alt={props.alt} height={props.height} />
  );
}

LogoLink.propTypes = {
  className: React.PropTypes.string,
  height: React.PropTypes.number,
  alt: React.PropTypes.string,
  home: React.PropTypes.bool,
};

export default LogoLink;
