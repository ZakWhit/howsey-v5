import React from 'react';
import { shallow } from 'enzyme';

import Logo from '../index';

const alt = 'test';
const renderComponent = (props = {}) => shallow(
  <Logo alt={alt} {...props} />
);

describe('<Logo />', () => {
  it('should have an alt attribute', () => {
    const renderedComponent = renderComponent();
    expect(renderedComponent.prop('alt')).toEqual(alt);
  });
});
