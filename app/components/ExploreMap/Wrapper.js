/**
 * Explore page content wrapper
 */

import styled from 'styled-components';

export default styled.div`
  display: inline-block;
  width: 100%;
  height: 100%;
`;
