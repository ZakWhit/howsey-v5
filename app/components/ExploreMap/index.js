/*
 * ExploreMap
 *
 * Google map component for exploring houses
 *
 */

import React, { PropTypes } from 'react';
import uuidv4 from 'uuid/v4'
import { getDistance } from 'geolib';

import RaisedButton from 'material-ui/RaisedButton';
import Marker from 'containers/MapMarker';
import Wrapper from './Wrapper';
import GoogleMapReact from './GoogleMapReact';

// Set marker size
const MARKER_SIZE = 22;

export class ExploreMap extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = {
      showSTA: false,
      coords: this.props.coords,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleTouchTap = this.handleTouchTap.bind(this);
  }

  handleChange(mapProps, dist) {
    const panDist = getDistance(
      {latitude: this.props.coords.lat, longitude: this.props.coords.lng},
      {latitude: mapProps.center.lat, longitude: mapProps.center.lng}
    )
    if (panDist > dist / 2) {
      this.setState({
        showSTA: true,
        coords: mapProps.center,
      });
    } else {
      this.setState({
        showSTA: false,
        coords: mapProps.center,
      });
    }
  }

  handleTouchTap() {
    this.props.searchThisArea(this.state.coords);
  }

  render() {
    let placesNearby = (null);
    let zoom = 15;
    let distDiff = 0;

    function radiusToZoom(radius){
      return Math.round(15-Math.log(radius/1609)/Math.LN2);
    }

    // If we have nearby places, render them
    if (this.props.placesNearby) {
      placesNearby = this.props.placesNearby.map((place, index) => (
          <Marker key={uuidv4()} dataId={index} place={place} lat={place.lat} lng={place.lng} />
        ));
      placesNearby.reverse();

      distDiff = this.props.placesNearby[this.props.placesNearby.length - 1].distance;
      zoom = radiusToZoom(distDiff);
    }

    const options = {
      mapTypeId: 'hybrid',
      mapTypeControl: false,
      rotateControl: false,
      tilt: 0,
    };

    return (
      <Wrapper>
        <GoogleMapReact options={options} onChange={(evt) => this.handleChange(evt, distDiff)} center={this.props.coords} zoom={zoom} hoverDistance={MARKER_SIZE / 2}>
          {placesNearby}
        </GoogleMapReact>
        {(this.state.showSTA) ?
            <RaisedButton
              label="SEARCH THIS AREA"
              style={{ position:'absolute', top: 15, left: 0, right: 0, margin: '0 auto', width: 160 }}
              primary
              onTouchTap={this.handleTouchTap}
            />
          : null}
      </Wrapper>
    );
  }
}

ExploreMap.propTypes = {
  coords: PropTypes.object,
  placesNearby: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.object,
    React.PropTypes.any,
  ]),
  searchThisArea: PropTypes.func,
};

export default ExploreMap;
