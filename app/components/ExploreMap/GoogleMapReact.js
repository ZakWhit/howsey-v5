/**
 * Explore page content wrapper
 */

import GoogleMapReact from 'google-map-react';
import styled from 'styled-components';

export default styled(GoogleMapReact)`
  display: inline-block;
  width: 100%;
  height: 100%;
`;
