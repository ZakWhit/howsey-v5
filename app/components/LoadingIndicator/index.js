import React from 'react';
import { FormattedMessage } from 'react-intl';

import CircularProgress from 'material-ui/CircularProgress';
import Wrapper from './Wrapper';

// Import messages
import messages from './messages';

function LoadingIndicator(props) {
  return (
    <Wrapper>
      <CircularProgress size={60} thickness={7} />
      <h3>{(props.text) ? props.text : <FormattedMessage {...messages.loading} />}</h3>
    </Wrapper>
  );
}

LoadingIndicator.propTypes = {
  text: React.PropTypes.string,
};

export default LoadingIndicator;
