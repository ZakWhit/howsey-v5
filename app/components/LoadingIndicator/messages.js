/*
 * HeaderToolBar Messages
 *
 * This contains all the text for the HeaderToolBar component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  loading: {
    id: 'boilerplate.components.LoadingIndicator.loading',
    defaultMessage: 'Loading...',
  },
});
