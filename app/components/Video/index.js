/**
 *
 * Video.react.js
 *
 * Renders a video tag
 *
 */

import React, { PropTypes } from 'react';

function Video(props) {
  return (
    <video
      id={props.id}
      className={props.className}
      height={props.height}
      loop={props.loop}
      preload={props.preload}
      autoPlay={props.autoPlay}
      muted={props.muted}
    >
      <source src={props.src} type="video/mp4" />
    </video>
  );
}

// We require the use of src and alt, only enforced by react in dev mode
Video.propTypes = {
  src: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]).isRequired,
  id: PropTypes.string,
  className: PropTypes.string,
  autoPlay: PropTypes.bool,
  height: PropTypes.string,
  loop: PropTypes.string,
  preload: PropTypes.string,
  muted: PropTypes.bool,
};

export default Video;
