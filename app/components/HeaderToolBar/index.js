/**
 *
 * HeaderToolBar
 *
 * Header toolbar component containing tools, menu items.
 * Takes properties for page and authentication state
 *
 */

import React from 'react';
import { browserHistory, Link } from 'react-router';
import { FormattedMessage } from 'react-intl';

// Import components
import { ToolbarGroup, ToolbarSeparator } from 'material-ui/Toolbar';
import AccountButton from 'components/AccountButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from './MenuItem';
import FlatButton from './FlatButton';
import SearchBarGroup from './SearchBarGroup';
import LocationSearchBar from './LocationSearchBar';

import { Tab } from 'material-ui/Tabs';
import MapIcon from 'material-ui/svg-icons/maps/place';

import HappyIcon from 'material-ui/svg-icons/social/mood';
import Help from 'material-ui/svg-icons/action/help';
import AccountCircle from 'material-ui/svg-icons/action/account-circle';
import HomeIcon from 'material-ui/svg-icons/action/home';
import Divider from 'material-ui/Divider';
import ReportIcon from 'material-ui/svg-icons/action/assessment';
import OwnerIcon from 'material-ui/svg-icons/action/face';

import Tabs from './Tabs';

import Wrapper from './Wrapper';
import Toolbar from './Toolbar';

// Import messages
import messages from './messages';

// Import inline-styles
import styles from './style';

// Define LocationSearchBar properties for AutoComplete and RaisedButton components
let autoCompleteProps = {
  menuCloseDelay: 300,
  openOnFocus: false,
  fullWidth: true,
  style: styles.autocomplete,
  textFieldStyle: styles.textfield,
  popoverProps: styles.popover,
};

const buttonProps = {
  style: styles.button,
  secondary: true,
};

class HeaderToolBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      flex: '0',
      border: '1px solid rgb(204, 204, 204)',
      bc: 'rgba(0, 0, 0, 0.05)',
      open:false,
    };

    this.onFocus = this.onFocus.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onClose = this.onClose.bind(this);
  }

  onFocus() {
    this.setState({
      flex: '1',
      border: '1px solid #d14f27',
      bc: 'rgba(0, 0, 0, 0)',
    });
  }

  onBlur(e) {
    if (e.relatedTarget && e.relatedTarget.className && e.relatedTarget.className === 'header-search-bar-menu-item') {
      e.preventDefault()
    } else {
      this.setState({
        flex: '0',
        border: '1px solid rgb(204, 204, 204)',
        bc: 'rgba(0, 0, 0, 0.05)',
      });
    }
  }

  onClose(e) {
    this.setState({
      flex: '0',
      border: '1px solid rgb(204, 204, 204)',
      bc: 'rgba(0, 0, 0, 0.05)',
    });
  }

  handleLogout() {
    this.setState({
      open: false,
    });
    this.props.handleLogout();
  }

  handleTouchTap = (event) => {
    // This prevents ghost click.
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  // Change route on tab change
  handleActive(tab) {
    browserHistory.push(tab.props['data-route']);
  }

  render() {
    autoCompleteProps.style.border = this.state.border;
    autoCompleteProps.style.backgroundColor = this.state.bc;
    let inkWidth = '93.03px';
    let inkLeft = 0;
    if (this.props.tabIndex === 0) {
      inkWidth = '93.03px';
      inkLeft = 0;
    } else if (this.props.tabIndex === 1) {
      inkWidth = '78.72px';
      inkLeft = 93.03;
    } else {
      inkWidth = '119.33px';
      inkLeft = 171.75;
    }

    const tabIndex = (this.props.pathname.indexOf('explore') === -1) ? 3 : this.props.tabIndex;

    return (
      <Wrapper className={this.props.className} pathname={this.props.pathname}>
        <Toolbar>
          <ToolbarGroup firstChild>
            <Tabs inkBarStyle={{ background: '#d14f27', width: inkWidth, left: inkLeft }} initialSelectedIndex={3} value={tabIndex}
                tabItemContainerStyle={{height:48}}
                tabTemplateStyle={{height:48}}
              >
              <Tab
                icon={<ReportIcon />}
                label={<FormattedMessage {...messages.tab1} />}
                data-route="/explore/report"
                onActive={this.handleActive}
                value={0}
                style={{fontSize: '14px', width:'auto'}}
                buttonStyle={{height:48, padding: '0 8px', marginRight: '6px', flexDirection: 'row', textTransform: 'capitalize'}}
              />
              <Tab
                icon={<MapIcon />}
                label={<FormattedMessage {...messages.tab2} />}
                data-route="/explore/map"
                onActive={this.handleActive}
                value={1}
                style={{fontSize: '14px', width:'auto'}}
                buttonStyle={{height:48, padding: '0 8px', marginRight: '6px', flexDirection: 'row', textTransform: 'capitalize'}}
              />
              <Tab
                icon={<OwnerIcon />}
                label={<FormattedMessage {...messages.tab3} />}
                data-route="/explore/owner-view"
                onActive={this.handleActive}
                value={2}
                style={{fontSize: '14px', width:'auto'}}
                buttonStyle={{height:48, padding: '0 8px', flexDirection: 'row', textTransform: 'capitalize'}}
              />
            </Tabs>
            <ToolbarSeparator style={{width: 0, marginLeft: '16px'}}/>
          </ToolbarGroup>
          {(this.props.pathname !== '/') ? (
            <SearchBarGroup>
              <LocationSearchBar
                flex={this.state.flex}
                onFocus={this.onFocus}
                onBlur={this.onBlur}
                onClose={this.onClose}
                className='header-search-bar'
                menuItemClass='header-search-bar-menu-item'
                labelText={<FormattedMessage {...messages.label} />}
                hintText={<FormattedMessage {...messages.placeholder} />}
                btnLabel={null}
                autoCompleteProps={autoCompleteProps}
                buttonProps={buttonProps}
                pathname={this.props.pathname}
              />
            <ToolbarSeparator style={{width: 0, marginLeft: '16px'}}/>
            </SearchBarGroup>
          ) :
            (<SearchBarGroup></SearchBarGroup>)
          }
          <ToolbarGroup lastChild>
            {(!this.props.authenticated) ? <FlatButton label={<FormattedMessage {...messages.signup} />} onTouchTap={() => this.props.handleOpen(true)} backgroundColor="#d14f27" hoverColor="#df7353" labelStyle={{color: '#fff', textTransform: 'capitalize', top: '-1px'}} style={{container:{borderRadius: "50px"}}}/>
              : null
            }
            {(!this.props.authenticated) ? <ToolbarSeparator style={{width: 0, marginLeft: '16px'}}/> : null }
            {(!this.props.authenticated) ? <FlatButton label={<FormattedMessage {...messages.signin} />} onTouchTap={() => this.props.handleOpen(false)} backgroundColor="#d14f27" hoverColor="#df7353" labelStyle={{color: '#fff', textTransform: 'capitalize', top: '-1px'}} style={{container:{borderRadius: "50px"}}}/>
              : null
            }
            {(this.props.authenticated) ? (
              <AccountButton
                onTouchTap={this.handleTouchTap}
              />
            ): null}
            {(this.props.authenticated) ? (
              <Popover
                open={this.state.open}
                anchorEl={this.state.anchorEl}
                anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                targetOrigin={{horizontal: 'left', vertical: 'top'}}
                onRequestClose={this.handleRequestClose}
                style={{marginTop: '8px'}}
              >
                <Menu>
                  <MenuItem primaryText="Profile" leftIcon={<AccountCircle />} />
                  <MenuItem primaryText="My Home" leftIcon={<HomeIcon />} />
                  <Divider />

                  <MenuItem primaryText="Help" leftIcon={<Help />} />
                  <Divider />
                  <MenuItem primaryText="Sign out" leftIcon={<HappyIcon />} onTouchTap={() => this.handleLogout()} />
                </Menu>
              </Popover>
            ): null}
          </ToolbarGroup>
        </Toolbar>
      </Wrapper>
    );
  }

}

HeaderToolBar.propTypes = {
  className: React.PropTypes.string,
  pathname: React.PropTypes.string,
  handleOpen: React.PropTypes.func,
  authenticated: React.PropTypes.bool,
  tabIndex: React.PropTypes.number,
  handleLogout: React.PropTypes.func,
};

export default HeaderToolBar;
