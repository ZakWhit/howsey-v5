// Inline styles for HeaderToolBar component

let styles = {
  autocomplete: {
    display: 'inline-block',
    boxSizing: 'border-box',
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    padding: '2px 15px',
    borderRadius: '50px',
    border: '1px solid rgb(204, 204, 204)',
  },
  textfield: {
    boxSizing: 'border-box',
    fontSize: '14px',
    paddingRight: '51px',
  },
  button: {
    position: 'absolute',
    top: 1,
    right: 1,
    bottom: 1,
    height: 32,
    width: 60,
    minWidth: 60,
    background: '#ccc',
    color: '#fff',
    borderTopRightRadius: '50px',
    borderBottomRightRadius: '50px',
    borderTopLeftRadius: '0px',
    borderBottomLeftRadius: '0px',
    backgroundColor: 'transparent',
  },
  popover: {
    style: {
      marginTop: '5px',
    },
  },
};

export default styles;
