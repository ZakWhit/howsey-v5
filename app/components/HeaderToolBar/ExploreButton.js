/**
 * Explore Map button component
 */

import FlatButton from 'material-ui/FlatButton';
import styled from 'styled-components';

export default styled(FlatButton)`
  color: ${(props) => (props.home) ? '#fff' : '' } !important;
  margin: 0 0 0 16px !important;
`;
