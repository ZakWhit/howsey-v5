/**
 * headerToolBar Wrapper
 */

import styled from 'styled-components';

 export default styled.div`
   & .menu-item:hover {
     background-color: rgba(0,0,0,0) !important;
   }
 `;

 // & .menu-item:hover {
 //   background-color: ${(props) => (props.pathname !== '/') ? '#fff ': props.theme.palette.primary1Color} !important;
 //   color: ${(props) => (props.pathname !== '/') ? props.theme.palette.primary1Color : '#fff'} !important;
 //   box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px;
 // }
