/**
 * LocationSearchBar component
 */

import LocationSearchBar from 'containers/LocationSearchBar';
import styled from 'styled-components';

export default styled(LocationSearchBar)`
  position: relative;
  display: block;
  flex-grow: ${(props) => props.flex};
  flex-shrink: 1;
  flex-basis: 370px;
  height: 34px;
  border-radius: 50px;
  transition: flex 0.2s ease;
  margin-left: auto;

  & input {
    display: inline-block;
    color: rgb(25, 118, 210) !important;
    text-shadow: 0px 0px 0px #000;
    -webkit-text-fill-color: transparent;
    transition: width 1s ease;
  }

  & input::-webkit-input-placeholder{
    color: #ccc;
    text-shadow: none;
    -webkit-text-fill-color: initial;
  }

  & hr {
    display: none;
  }

  & button {
    border-top-right-radius: 50px !important;
    border-bottom-right-radius: 50px !important;
    border-top-left-radius: 0px !important;
    border-bottom-left-radius: 0px !important;
  }

  & .autocomplete {
    height: 28px !important;
  }

  & .autocomplete div:first-of-type {
    bottom: 0 !important;
    line-height: 27px !important;
  }
`;
