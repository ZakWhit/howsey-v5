/**
 * MenuItem component
 */

import FlatButton from 'material-ui/FlatButton';
import styled from 'styled-components';

export default styled(FlatButton)`
  border-radius: 50px !important;
`;
