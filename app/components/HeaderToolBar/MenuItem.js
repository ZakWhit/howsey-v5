/**
 * MenuItem component
 */

import MenuItem from 'material-ui/MenuItem';
import styled from 'styled-components';

export default styled(MenuItem)`
  font-size: 14px !important;
`;
