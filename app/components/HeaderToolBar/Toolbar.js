/**
 * Material-UI ToolBar component
 */


import { Toolbar } from 'material-ui/Toolbar';
import styled from 'styled-components';

export default styled(Toolbar)`
  background: transparent !important;
`;
