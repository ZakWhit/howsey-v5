/**
 * MenuItem component
 */

import { ToolbarGroup } from 'material-ui/Toolbar';
import styled from 'styled-components';

export default styled(ToolbarGroup)`
  flex: 1 0 0%;
  text-align: left;
`;
