/**
 * ExploreMenu Tabs
 */

import {Tabs} from 'material-ui/Tabs';
 import styled from 'styled-components';

 export default styled(Tabs)`
    width: auto;
    & svg {
      margin-bottom: 0 !important;
      margin-right: 5px !important;
    }
 `;
