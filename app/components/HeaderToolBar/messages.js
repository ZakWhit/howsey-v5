/*
 * HeaderToolBar Messages
 *
 * This contains all the text for the HeaderToolBar component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  label: {
    id: 'boilerplate.containers.LandingPage.label',
    defaultMessage: 'Enter your address',
  },
  placeholder: {
    id: 'boilerplate.containers.LandingPage.placeholder',
    defaultMessage: 'Where do you call home?',
  },
  btnlabel: {
    id: 'boilerplate.containers.LandingPage.btnlabel',
    defaultMessage: 'Search',
  },
  explorebtn: {
    id: 'boilerplate.components.HeaderToolBar.explorebtn',
    defaultMessage: 'Explore Map',
  },
  signup: {
    id: 'boilerplate.components.HeaderToolBar.signup',
    defaultMessage: 'Sign up',
  },
  signin: {
    id: 'boilerplate.components.HeaderToolBar.signin',
    defaultMessage: 'Log in',
  },
  logout: {
    id: 'boilerplate.components.HeaderToolBar.logout',
    defaultMessage: 'Logout',
  },
  tab1: {
    id: 'boilerplate.components.ExploreMenu.tab1',
    defaultMessage: 'Report',
  },
  tab2: {
    id: 'boilerplate.components.ExploreMenu.tab2',
    defaultMessage: 'Map',
  },
  tab3: {
    id: 'boilerplate.components.ExploreMenu.tab3',
    defaultMessage: 'Owner View',
  },
});
