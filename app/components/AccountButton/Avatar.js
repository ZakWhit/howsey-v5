/**
 * Avatar component
 */

import Avatar from 'material-ui/Avatar';
import styled from 'styled-components';

export default styled(Avatar)`
  transition: background-color 0.6s ease;
  &:hover {
    background-color: #0077b3 !important;
  }
`;
