import React from 'react';

import Paper from 'material-ui/Paper';
import Avatar from './Avatar';

function AccountButton(props) {
  return (
    <Paper zDepth={1} circle style={{backgroundColor: 'transparent'}}>
      <Avatar onTouchTap={props.onTouchTap} size={34} backgroundColor='#006699' style={{cursor: 'pointer'}}>Z</Avatar>
    </Paper>
  );
}

AccountButton.propTypes = {
  className: React.PropTypes.string,
  user: React.PropTypes.object,
  onTouchTap: React.PropTypes.func,
};

export default AccountButton;
