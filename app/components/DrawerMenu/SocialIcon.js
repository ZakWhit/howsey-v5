/**
 * DrawerMenu footer social icons
 */

import { SocialIcon } from 'react-social-icons';
import styled from 'styled-components';

export default styled(SocialIcon)`
  margin: 10px 10px;
  cursor: pointer;
  height: 30px !important;
  width: 30px !important;
`;
