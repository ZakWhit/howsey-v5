/**
 * DrawerMenu social icons wrapper
 */

import Paper from 'material-ui/Paper';
import styled from 'styled-components';

export default styled(Paper)`
  position: fixed;
  bottom: 0px;
  left: 0px;
  width: 100%;
  text-align: center;
  background-color: ${(props) => props.theme.palette.primary3Color} !important;
  border-radius: 0 !important;
  z-index: 10;
`;
