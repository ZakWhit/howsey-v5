/**
 * DrawerMenu logo component
 */

import Logo from 'components/Logo';
import styled from 'styled-components';

export default styled(Logo)`
  margin: 10px 0 10px 15px;
`;
