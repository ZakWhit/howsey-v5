/**
 * DrawerMenu main wrapper Div
 */

import styled from 'styled-components';

export default styled.div`
  position: relative;
  height: 100%;
  overflow-y: scroll;
`;
