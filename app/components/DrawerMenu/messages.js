/*
 * DrawerMenu Messages
 *
 * This contains all the text for the DrawerMenu component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  headerplace: {
    id: 'boilerplate.components.DrawerMenu.headerplace',
    defaultMessage: 'Find a place',
  },
  menusearch: {
    id: 'boilerplate.components.DrawerMenu.menusearch',
    defaultMessage: 'Search',
  },
  headerrealestate: {
    id: 'boilerplate.components.DrawerMenu.headerrealestate',
    defaultMessage: 'Real Estate',
  },
  menuagent: {
    id: 'boilerplate.components.DrawerMenu.menuagent',
    defaultMessage: 'Find an agent',
  },
  menuhome: {
    id: 'boilerplate.components.DrawerMenu.menuhome',
    defaultMessage: 'List my home',
  },
  headerhelp: {
    id: 'boilerplate.components.DrawerMenu.headerhelp',
    defaultMessage: 'Help',
  },
  menuhelpdesk: {
    id: 'boilerplate.components.DrawerMenu.menuhelpdesk',
    defaultMessage: 'Help desk',
  },
  menufaqs: {
    id: 'boilerplate.components.DrawerMenu.menufaqs',
    defaultMessage: 'FAQs',
  },
  headerprofile: {
    id: 'boilerplate.components.DrawerMenu.headerprofile',
    defaultMessage: 'Profile',
  },
  menusignin: {
    id: 'boilerplate.components.DrawerMenu.menusignin',
    defaultMessage: 'Sign in',
  },
  menusignup: {
    id: 'boilerplate.components.DrawerMenu.menusignup',
    defaultMessage: 'Sign up',
  },
});
