/**
 * Material-UI menu divider with no margins
 */

import Divider from 'material-ui/Divider';
import styled from 'styled-components';

export default styled(Divider)`
  margin-top: 0px;
  margin-bottom: 0px;
`;
