/**
 * Menu component
 */

import Menu from 'material-ui/Menu';
import styled from 'styled-components';

export default styled(Menu)`
  padding: 60px 0 50px 0 !important;
`;
