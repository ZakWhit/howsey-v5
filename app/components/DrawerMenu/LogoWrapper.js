/**
 * DrawerMenu logo wrapper component
 */

import Paper from 'material-ui/Paper';
import styled from 'styled-components';

export default styled(Paper)`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 60px;
  background-color: ${(props) => props.theme.palette.primary1Color} !important;
  border-radius: 0 !important;
  z-index: 10;
`;
