/**
 *
 * DrawerMenu
 *
 * Menu, drawer header/footer components found in the main left-hand side-bar
 *
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';

// Import menu icons
import Home from 'material-ui/svg-icons/action/home';
import Account from 'material-ui/svg-icons/action/account-box';
import Faqs from 'material-ui/svg-icons/action/question-answer';
import Help from 'material-ui/svg-icons/action/help';
import Search from 'material-ui/svg-icons/image/remove-red-eye';
import MapIcon from 'material-ui/svg-icons/maps/map';

// Import components
import MenuItem from 'material-ui/MenuItem';
import Subheader from 'material-ui/Subheader';
import Wrapper from './Wrapper';
import Logo from './Logo';
import Divider from './Divider';
import LogoWrapper from './LogoWrapper';
import Menu from './Menu';
import SocialIcon from './SocialIcon';
import SocialWrapper from './SocialWrapper';

// Import messages
import messages from './messages';

function DrawerMenu() {
  return (
    <Wrapper>
      <LogoWrapper zDepth={1}>
        <Logo home alt={'howsey-logo-drawer'} height={40} />
      </LogoWrapper>
      <Menu width={300}>
        <Subheader><FormattedMessage {...messages.headerplace} /></Subheader>
        <MenuItem primaryText={<FormattedMessage {...messages.menusearch} />} leftIcon={<MapIcon />} />
        <Divider />
        <Subheader><FormattedMessage {...messages.headerrealestate} /></Subheader>
        <MenuItem primaryText={<FormattedMessage {...messages.menuagent} />} leftIcon={<Search />} />
        <MenuItem primaryText={<FormattedMessage {...messages.menuhome} />} leftIcon={<Home />} />
        <Divider />
        <Subheader><FormattedMessage {...messages.headerhelp} /></Subheader>
        <MenuItem primaryText={<FormattedMessage {...messages.menuhelpdesk} />} leftIcon={<Help />} />
        <MenuItem primaryText={<FormattedMessage {...messages.menufaqs} />} leftIcon={<Faqs />} />
        <Divider />
        <Subheader><FormattedMessage {...messages.headerprofile} /></Subheader>
        <MenuItem primaryText={<FormattedMessage {...messages.menusignin} />} leftIcon={<Account />} />
        <MenuItem primaryText={<FormattedMessage {...messages.menusignup} />} leftIcon={<Account />} />
      </Menu>
      <SocialWrapper zDepth={1}>
        <div>
          <SocialIcon network="facebook" color={'#ffffff'} />
          <SocialIcon network="twitter" color={'#ffffff'} />
          <SocialIcon network="instagram" color={'#ffffff'} />
        </div>
      </SocialWrapper>
    </Wrapper>
  );
}

export default DrawerMenu;
