/**
 *
 * LogoLink
 *
 * Renders the Howsey logo as a link to the landing page
 *
 */

import React from 'react';
import { Link } from 'react-router';

// Import components
import Logo from 'components/Logo';

function LogoLink(props) {
  return (
    <Link className={props.className} to="/">
      <Logo alt={'howsey-logo-home'} height={props.height} home={props.home} />
    </Link>
  );
}

LogoLink.propTypes = {
  className: React.PropTypes.string,
  height: React.PropTypes.number,
  home: React.PropTypes.bool,
};

export default LogoLink;
