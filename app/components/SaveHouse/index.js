/*
 * SaveHouse Component
 *
 * Saves the current state place to the user profile
 *
 */

import React, { PropTypes } from 'react';

// Import Components
import FavBorderIcon from 'material-ui/svg-icons/action/favorite-border';
import FavIcon from 'material-ui/svg-icons/action/favorite';
import Snackbar from 'material-ui/Snackbar';
import IconButton from 'material-ui/IconButton';

class SaveHouse extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      message: 'Property saved to your profile!',
      open: false,
      liked: false,
      hover: false,
    }

    this.handleMouseOver = this.handleMouseOver.bind(this);
    this.handleMouseOut = this.handleMouseOut.bind(this);
    this.handleTouchTap = this.handleTouchTap.bind(this);
    this.handleActionTouchTap = this.handleActionTouchTap.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
  }

  handleMouseOver() {
    this.setState({
      hover: true,
    });
  }

  handleMouseOut() {
    this.setState({
      hover: false,
    });
  }

  handleTouchTap = () => {
    const message = (this.state.liked) ? 'Property removed from your profile!': 'Property saved to your profile!';
    this.setState({
      open: true,
      message: message,
      liked: !this.state.liked
    });
  };

  handleActionTouchTap = () => {
    this.setState({
      open: false,
      liked: !this.state.liked
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  render() {
    const favColor = (this.state.hover || this.state.liked) ? 'rgb(209, 79, 39)' : 'rgba(209, 79, 39,0)';
    const tooltipPosition = (this.props.tooltipTop) ? 'top-center' : 'bottom-center';

    return (
      <div
        className={this.props.className}

      >
        <IconButton
          tooltip='Save'
          tooltipPosition={tooltipPosition}
          onMouseOver={this.handleMouseOver}
          onMouseOut={this.handleMouseOut}
          onTouchTap={this.handleTouchTap}
          style={{position: 'absolute', top:5, left:5, height: 38, width: 38, zIndex:2}}
          iconStyle={{position: 'absolute', top:0, left:0, height: 38, width: 38, zIndex:2}}
        >
          <FavBorderIcon color={'rgba(0,0,0,0.7)'} />
        </IconButton>
        <FavIcon color={favColor} style={{position: 'absolute', top:6, left:6, height: 35, width: 35, zIndex:1}} />
        {(this.props.snackbar) ?
          <Snackbar
            open={this.state.open}
            message={this.state.message}
            action="UNDO"
            autoHideDuration={3000}
            onActionTouchTap={this.handleActionTouchTap}
            onRequestClose={this.handleRequestClose}
          />
          : null
        }
      </div>
    );
  }
}

SaveHouse.propTypes = {
  className: PropTypes.string,
  snackbar: PropTypes.bool,
  tooltipTop: PropTypes.bool,
};


export default SaveHouse;
