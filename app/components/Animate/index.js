/**
 *
 * Animate Component
 *
 * Wrap elements with this component that you wish to animate
 */

import React, { PropTypes } from 'react';
import AnimateWrapper from './AnimateWrapper';

function Animate(props) {
  return (
    <AnimateWrapper className={props.className} duration={props.duration} transform={props.transform}>
      {React.Children.toArray(props.children)}
    </AnimateWrapper>
  );
}

// We require the use of src and alt, only enforced by react in dev mode
Animate.propTypes = {
  className: PropTypes.string,
  duration: PropTypes.string,
  transform: PropTypes.string,
  children: React.PropTypes.node,
};

export default Animate;
