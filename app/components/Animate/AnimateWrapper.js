/**
 * Animate Wrapper
 */

import styled from 'styled-components';

export default styled.div`
  position: relative;
  transition-timing-function: ease;
  transition-property: transform;
  transition-duration: ${(props) => props.duration};
  transform: ${(props) => props.transform};
`;
