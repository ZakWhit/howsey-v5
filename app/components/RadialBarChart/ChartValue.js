/**
 * Explore page dashboard paper component
 */

import styled from 'styled-components';

export default styled.p`
  position: absolute;
  lineHeight: ${(props) => props.height};
  top: 0;
  left: 0;
  right: 0;
  margin: 0 auto;
  fontSize: 1.75em;
  color: ${(props) => props.color};
  fontWeight: bold;
  textAlign: center;
`;
