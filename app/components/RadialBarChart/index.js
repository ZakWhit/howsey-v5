/**
 *
 * HighchartsRadial Bar Chart Component with wrapper
 *
 */

import React from 'react';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';

// Import components
import SolidGauge from 'highcharts/modules/solid-gauge';
import ChartValue from './ChartValue';
import ChartWrapper from './ChartWrapper';

HighchartsMore(ReactHighcharts.Highcharts);
SolidGauge(ReactHighcharts.Highcharts);

class RadialBarChart extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ChartWrapper height={this.props.height} width={this.props.height}>
        <ReactHighcharts config={this.props.config} neverReflow></ReactHighcharts>
        <ChartValue height={this.props.height} color={this.props.barColor}>{this.props.riskScore}%</ChartValue>
      </ChartWrapper>
    );
  }
}

RadialBarChart.propTypes = {
  config: React.PropTypes.object,
  riskScore: React.PropTypes.number,
  barColor: React.PropTypes.string,
  riskMessage: React.PropTypes.string,
  height: React.PropTypes.string,
  width: React.PropTypes.string,
};

export default RadialBarChart;
