/**
 * Explore page dashboard paper component
 */

import styled from 'styled-components';

export default styled.div`
  position: relative;
  width: ${(props) => props.width};
  height: ${(props) => props.height};
`;
