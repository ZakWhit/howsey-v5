/**
 *
 * LocationSearchBaritem
 *
 * Renders the markup for the content of the location AutoComplete MenuItem
 *
 */

import React, { PropTypes } from 'react';

function LocationSearchBarItem(props) {
  if (props.item.structured_formatting && props.item.structured_formatting.main_text && props.item.structured_formatting.secondary_text) {
    return (
      <div>
        <span style={{ fontWeight: 500 }}>{props.item.structured_formatting.main_text}&nbsp;&nbsp;</span>
        <small style={{ fontWeight: 400 }}>{props.item.structured_formatting.secondary_text}</small>
      </div>
    );
  }

  return (
    <div>
      <span style={{ fontWeight: 500 }}>{props.item.description}</span>
    </div>
  );
}

// We require the use of src and alt, only enforced by react in dev mode
LocationSearchBarItem.propTypes = {
  item: PropTypes.object.isRequired,
};

export default LocationSearchBarItem;
