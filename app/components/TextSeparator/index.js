/**
 *
 * TextSeparator
 *
 * Renders a divider split by some text
 * ---------- TEXT ----------
 *
 */

import React, { PropTypes } from 'react';

// Import components
import Wrapper from './Wrapper';
import Line from './Line';
import Text from './Text';

function TextSeparator(props) {
  return (
    <Wrapper width={props.width}>
      <Line />
      <Text>{props.text}</Text>
      <Line />
    </Wrapper>
  );
}

TextSeparator.propTypes = {
  width: PropTypes.string,
  text: PropTypes.string,
};

export default TextSeparator;
