/**
 * Divider text component
 */

import styled from 'styled-components';

export default styled.div`
  padding: 0 10px;
  font-size: 11px;
  font-weight: 700;
`;
