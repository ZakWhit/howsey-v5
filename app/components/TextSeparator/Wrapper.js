/**
 * TextSeparator wrapper div
 */

import styled from 'styled-components';

export default styled.div`
  width: ${(props) => props.width}
  margin: 0;
  display: flex;
  align-items: center;
`;
