/**
 * Divider line component
 */

import styled from 'styled-components';

export default styled.div`
  flex: 1 0 auto;
  min-width: 1px;
  border-top: 1px solid #d3d3d3;
  height: 1px;
`;
