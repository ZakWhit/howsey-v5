/**
 *
 * SignUpForm
 *
 * Sign up form container
 *
 */

 /* eslint-disable no-unused-vars */
 /* eslint-disable react/prop-types */

import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form/immutable';
import { FormattedMessage } from 'react-intl';


// Import components
import TextField from 'material-ui/TextField';
import SubmitButton from './SubmitButton';

// Import messages
import messages from './messages';

const renderTextField = ({ input, label, meta: { touched, error }, ...custom }) => (
  <TextField
    hintText={label}
    errorText={touched && error}
    fullWidth
    {...input}
    {...custom}
  />
);

class LoginForm extends Component {
  constructor() {
    super();
  }

  renderErrors(errors) {
    return (
      <div className="alert alert-danger" role="alert">
        {errors.map((error, index) => <span key={index}>{error.value}</span>)}
      </div>
    );
  }

  render() {
    const { handleSubmit, pristine, reset, submitting, } = this.props;

    return (
      <form className={this.props.className} style={this.props.style} onSubmit={handleSubmit}>
        {(this.props.errors <= 0) ? null : this.renderErrors(this.props.errors)}
        <div>
          <Field name="email" component={renderTextField} type="text" label={<FormattedMessage {...messages.email} />} />
        </div>
        <div>
          <Field name="password" component={renderTextField} type="password" label={<FormattedMessage {...messages.password} />} />
        </div>
        <div className="errors">
        </div>
        <SubmitButton
          action="submit"
          type="submit"
          label={<FormattedMessage {...messages.submit} />}
          primary
          labelColor="#FFFFFF"
          disabled={submitting}
        />
      </form>
    );
  }
}

LoginForm.propTypes = {
  className: React.PropTypes.string,
  style: React.PropTypes.object,
  handleSubmit: React.PropTypes.func,
  onSubmit: React.PropTypes.func,
  errors: React.PropTypes.any,
};

const validate = (values) => {
  const errors = {}

  if (!values.email) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if (!values.password) {
    errors.password = 'Required';
  } else if (values.password.length <= 3) {
    errors.password = 'Must be at least 4 characters';
  }

  return errors;
}

export default reduxForm({
  form: 'LogInForm', // a unique name for this form
  validate
})(LoginForm);
