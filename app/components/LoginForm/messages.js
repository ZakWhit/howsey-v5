/*
 * LoginForm Messages
 *
 * This contains all the text for the LoginForm component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  email: {
    id: 'boilerplate.components.LoginForm.email',
    defaultMessage: 'Email',
  },
  password: {
    id: 'boilerplate.components.LoginForm.password',
    defaultMessage: 'Password',
  },
  submit: {
    id: 'boilerplate.components.LoginForm.submit',
    defaultMessage: 'Log In',
  },
});
