import React from 'react';

function IconLabel(props) {


  return (
    <div style={{display:'flex',flexDirection:'row', alignItems: 'center'}}>
      {props.icon}
      <div style={{marginLeft:16}}>
        <p style={{margin:0, fontSize: '1em', lineHeight: '1.2em', fontWeight: 500}}>{props.title}</p>
        <p style={{margin:0, fontSize: '1em', lineHeight: '1.2em', fontWeight: 300}}>{props.subtitle}</p>
      </div>
    </div>
  );
}

IconLabel.propTypes = {
  icon: React.PropTypes.node,
  title: React.PropTypes.string,
  subtitle: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    React.PropTypes.string,
    React.PropTypes.number,
  ]),
};

export default IconLabel;
