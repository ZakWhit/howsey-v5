/**
 *
 * Header
 *
 * App header containing menu-icon toggle, logo, and toolbar
 *
 */

import React, { Component } from 'react';

// Import components
import Drawer from 'material-ui/Drawer';
import { Row } from 'components/FlexboxGrid';
import LogoLink from 'components/LogoLink';
import DrawerMenu from 'components/DrawerMenu';
import HeaderToolBar from 'containers/HeaderToolBar';
import Wrapper from './Wrapper';
import AppBar from './AppBar';

class Header extends Component {
  constructor(props) {
    super(props);

    this.handleToggle = this.handleToggle.bind(this);

    this.state = {
      open: false,
    };
  }

  // Handle DrawerMenu states
  handleToggle() {
    this.setState({ open: !this.state.open });
  }

  handleClose() {
    this.setState({ open: false });
  }

  render() {
    const appBarDepth = (this.props.pathname === '/') ? 0 : 1;
    const logoHome = (this.props.pathname === '/') ? true : false;
    return (
      <Wrapper pathname={this.props.pathname}>
        <Row>
          <AppBar
            className='appbar'
            title={<LogoLink home={logoHome} height={30} />}
            onLeftIconButtonTouchTap={this.handleToggle}
            iconElementRight={<HeaderToolBar pathname={this.props.pathname} />}
            iconStyleRight={{ margin: 0, flex: '1 1 0%', }}
            titleStyle={{ flex: '0 1 154px' }}
            zDepth={appBarDepth}
          />
        </Row>
        <Drawer
          docked={false}
          width={300}
          open={this.state.open}
          onRequestChange={(open) => this.setState({ open })}
        >
          <DrawerMenu />
        </Drawer>
      </Wrapper>
    );
  }
}

Header.propTypes = {
  pathname: React.PropTypes.string,
};

export default Header;
