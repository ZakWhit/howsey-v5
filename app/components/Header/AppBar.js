/**
 * Header Material-UI AppBar component
 */

import AppBar from 'material-ui/AppBar';
import styled from 'styled-components';

export default styled(AppBar)`
  padding-left: 15px;
  padding-right: 0;
`;
