/**
 * Header wrapper div
 */

import styled from 'styled-components';

export default styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  zIndex: 200;
`;

// & .appbar {
//   background-color: ${props => (props.pathname === '/') ? 'transparent !important' : 'initial' };
//   box-shadow: ${props => (props.pathname === '/') ? 'none !important' : 'initial' };
// }
