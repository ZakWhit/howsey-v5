/*
 *
 * Global Sagas Index
 *
 */

import { geoData } from 'containers/App/sagas';
 import { authData } from 'containers/AuthPage/sagas';

export default [
  geoData,
  authData
];
