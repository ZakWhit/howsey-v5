import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html {
    height: 100%;
    width: 100%;
    overflow: hidden;
  }

  body {
    height: 100%;
    width: 100%;
    overflow: auto;
    color: #606060;
    font-family: 'Roboto', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Roboto, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }

  .hidden {
    display: none;
  }

  @media screen and (max-width: 1199px) {
    .container {
      max-width: 960px !important;
    }
  }

  @media screen and (max-width: 991px) {
    .container {
      max-width: 720px !important;
    }
  }

  @media screen and (max-width: 767px) {
    .container {
      max-width: 540px !important;
    }
  }

  @media screen and (max-width: 575px) {
    .container {
      max-width: 575px !important;
    }
  }
`;
