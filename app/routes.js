// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { browserHistory } from 'react-router';
import { getAsyncInjectors } from 'utils/asyncInjectors';
import { logoutAndRedirect } from 'containers/AuthPage/actions';
import { changeExploreTab } from 'containers/ExplorePage/actions';
import { changeReportTab } from 'containers/ExploreReportView/actions'


const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  return [
    {
      path: '/',
      name: 'home',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HomePage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'explore',
      name: 'explorePage',
      getComponent(location, cb) {
        const importModules = Promise.all([
          import('containers/ExplorePage/reducer'),
          import('containers/ExplorePage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, component]) => {
          injectReducer('explore', reducer.default);

          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
      indexRoute: {
        onEnter() {
          browserHistory.push('/explore/report/value');
        },
      },
      childRoutes: [
        {
          path: 'report',
          name: 'explorePageReport',
          getComponent(location, cb) {
            import('containers/ExploreReportView')
              .then(loadModule(cb))
              .catch(errorLoading);
          },
          onEnter() {
            store.dispatch(changeExploreTab(0));
          },
          indexRoute: {
            onEnter() {
              browserHistory.push('/explore/report/value');
            },
          },
          childRoutes: [
            {
              path: 'value',
              name: 'explorePageReportValue',
              getComponent(location, cb) {
                import('containers/ReportSlides/SlideOne')
                  .then(loadModule(cb))
                  .catch(errorLoading);
              },
              onEnter() {
                store.dispatch(changeReportTab(0));
              },
            },
            {
              path: 'risk',
              name: 'explorePageReportRisk',
              getComponent(location, cb) {
                import('containers/ReportSlides/SlideTwo')
                  .then(loadModule(cb))
                  .catch(errorLoading);
              },
              onEnter() {
                store.dispatch(changeReportTab(1));
              },
            },
            {
              path: 'neighbourhood',
              name: 'explorePageReportNeigh',
              getComponent(location, cb) {
                import('containers/ReportSlides/SlideThree')
                  .then(loadModule(cb))
                  .catch(errorLoading);
              },
              onEnter() {
                store.dispatch(changeReportTab(2));
              },
            },
            {
              path: 'insurance',
              name: 'explorePageReportInsure',
              getComponent(location, cb) {
                import('containers/ReportSlides/SlideTwo')
                  .then(loadModule(cb))
                  .catch(errorLoading);
              },
              onEnter() {
                store.dispatch(changeReportTab(3));
              },
            },
            {
              path: 'recommendations',
              name: 'explorePageReportRecs',
              getComponent(location, cb) {
                import('containers/ReportSlides/SlideTwo')
                  .then(loadModule(cb))
                  .catch(errorLoading);
              },
              onEnter() {
                store.dispatch(changeReportTab(4));
              },
            },
          ],
        },
        {
          path: 'map',
          name: 'explorePageMap',
          getComponent(location, cb) {
            import('containers/ExploreMapView')
              .then(loadModule(cb))
              .catch(errorLoading);
          },
          onEnter() {
            store.dispatch(changeExploreTab(1));
          },
        },
        {
          path: 'owner-view',
          name: 'explorePageReport',
          getComponent(location, cb) {
            import('containers/ExploreReportView')
              .then(loadModule(cb))
              .catch(errorLoading);
          },
          onEnter() {
            store.dispatch(changeExploreTab(2));
          },
        },
      ],
    }, {
      path: 'logout',
      name: 'Logout',
      onEnter() {
        store.dispatch(logoutAndRedirect());
      },
    }, {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        import('containers/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
  ];
}
