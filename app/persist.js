/*
 *
 * Helper functions for peristing the state in local storage
 *
 */

import transit from 'transit-immutable-js';
import throttle from 'lodash/throttle';

// load state from local storage
export const loadState = () => {
  try {
    const serializedState = transit.fromJSON(localStorage.getItem('state'));
    if (serializedState === null) {
      return {};
    }
    return serializedState;
  } catch (err) {
    return {};
  }
};

// save state to local storage
const saveState = (state, blacklist = []) => {

  try {
    let minState = state;
    blacklist.forEach((id) => { minState = minState.delete(id); });
    const serializedState = transit.toJSON(minState);
    localStorage.setItem('state', serializedState);
  } catch (err) {
    console.log(err);
    // Ignore write errors.
  }
};

// listen for state changes and save to local storage
export const persistStore = (store, blacklist) =>
  store.subscribe(throttle(() => saveState(store.getState(),blacklist), 777));
